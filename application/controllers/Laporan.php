<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'functions.php';

class Laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('beasiswa_data');
        $this->load->database();
        $this->load->helper(array('form', 'url', 'download'));
    }

    public function laporan_ppa()
    {
        include_once APPPATH . '/third_party/fpdf/fpdf.php';
        error_reporting(0);
        $pdf = new FPDF('L', 'cm', 'A4');
        $pdf->AddPage();
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Image('assets/images/logo_ummy.png', 1, 1, 2, 2); // pemanggilan logo
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN', 0, 'C');
        $pdf->SetFont('Times', 'B', 11);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Pendaftaran Beasiswa Online', 0, 'C');
        $pdf->SetFont('Times', 'B', 9);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Jl. Jend. Sudirman No.6, Kp.Jawa Kota Solok', 0, 'C');
        $pdf->SetFont('Times', 'B', 8);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Telp. (0755) 20565', 0, 'C');
        $pdf->Line(1, 3.1, 28.5, 3.1);
        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 3.2, 28.5, 3.2);
        $pdf->SetLineWidth(0);
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 12);
        $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $waktu[1] = date("d", time());
        $waktu[2] = date("m", time());
        $waktu[3] = date("Y", time());
        $tanggalini = "$waktu[1] " . $bulan[$waktu[2] - 1] . " $waktu[3]";
        $pdf->MultiCell(27, 0.5, ' Solok, ' . $tanggalini, 0, 'R');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 14);
        $pdf->MultiCell(27, 1.5, 'Laporan Pendaftar Beasiswa PPA ', 0, 'C');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->setFillColor(222, 222, 222);
        $pdf->Cell(1.0, 0.8, 'No', 1, 0);
        $pdf->Cell(4.0, 0.8, 'NPM ', 1, 0);
        $pdf->Cell(4.8, 0.8, 'Nama Mahasiswa ', 1, 0);
        $pdf->Cell(3.2, 0.8, 'Jenis Kelamin', 1, 0);
        $pdf->Cell(6.5, 0.8, 'Jurusan', 1, 0);
        $pdf->Cell(2.0, 0.8, 'IPK', 1, 0);
        $pdf->Cell(2.0, 0.8, 'Semester', 1, 0);
        $pdf->Cell(3.5, 0.8, 'Ta.Akademik', 1, 0);
        $pdf->SetFont('Times', 'B', 12);
        $no = 1;
        $tabel_ppa = $this->beasiswa_data->beasiswappa()->result();
        foreach ($tabel_ppa as $m) {
            $pdf->Ln();
            $pdf->Cell(1.0, 0.8, $no++, 1, 0);
            $pdf->Cell(4.0, 0.8, $m->npm, 1, 0);
            $pdf->Cell(4.8, 0.8, $m->nama_mahasiswa, 1, 0);
            $pdf->Cell(3.2, 0.8, $m->jenis_kelamin, 1, 0);
            $pdf->Cell(6.5, 0.8, $m->prodi, 1, 0);
            $pdf->Cell(2.0, 0.8, $m->ipk, 1, 0);
            $pdf->Cell(2.0, 0.8, $m->semester, 1, 0);
            $pdf->Cell(3.5, 0.8, $m->tahun_akademik, 1, 0);
        }
        $pdf->SetFont('Times', 'B', 12);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sp) {
            $pdf->MultiCell(23.5, 0.5, 'Diketahui', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(25, 0.5, $sp->nama_dekan, 0, 'R');
        }
        $pdf->Output();
    }


    public function laporan_bidikmisi()
    {
        include_once APPPATH . '/third_party/fpdf/fpdf.php';
        error_reporting(0);
        $pdf = new FPDF('L', 'cm', 'a4');
        $pdf->AddPage();
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Image('assets/images/logo_ummy.png', 1, 1, 2, 2); // pemanggilan logo
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN', 0, 'C');
        $pdf->SetFont('Times', 'B', 11);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Pendaftaran Beasiswa Online', 0, 'C');
        $pdf->SetFont('Times', 'B', 9);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Jl. Jend. Sudirman No.6, Kp.Jawa Kota Solok', 0, 'C');
        $pdf->SetFont('Times', 'B', 8);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Telp. (0755) 20565', 0, 'C');
        $pdf->Line(1, 3.1, 28.5, 3.1);
        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 3.2, 28.5, 3.2);
        $pdf->SetLineWidth(0);
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 12);
        $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $waktu[1] = date("d", time());
        $waktu[2] = date("m", time());
        $waktu[3] = date("Y", time());
        $tanggalini = "$waktu[1] " . $bulan[$waktu[2] - 1] . " $waktu[3]";
        $pdf->MultiCell(27, 0.5, ' Solok, ' . $tanggalini, 0, 'R');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 14);
        $pdf->MultiCell(27, 1.5, 'Laporan Pendaftar Beasiswa Bidikmisi ', 0, 'C');

        $pdf->SetFont('Times', 'B', 8);
        $pdf->setFillColor(222, 222, 222);
        $pdf->Cell(0.7, 0.8, 'No', 1, 0);
        $pdf->Cell(2.6, 0.8, 'No.Pendaftaran ', 1, 0);
        $pdf->Cell(2.9, 0.8, 'Nama Lengkap ', 1, 0);
        $pdf->Cell(2.0, 0.8, 'Jenis Kelamin', 1, 0);
        $pdf->Cell(2.1, 0.8, 'Tanggal Lahir', 1, 0);
        $pdf->Cell(2.4, 0.8, 'Alamat', 1, 0);
        $pdf->Cell(2.1, 0.8, 'J.Tanggungan', 1, 0);
        $pdf->Cell(1.7, 0.8, 'P.Keluarga', 1, 0);
        $pdf->Cell(1.9, 0.8, 'Tahun Tamat', 1, 0);
        $pdf->Cell(3.6, 0.8, 'Asal Sekolah', 1, 0);
        $pdf->Cell(1.7, 0.8, 'N.Rata-rata', 1, 0);
        $pdf->Cell(4.4, 0.8, 'Pilihan 1 (Utama)', 1, 0);
        $pdf->SetFont('Times', 'B', 8);
        $no = 1;
        $tabel_bidikmisi = $this->beasiswa_data->beasiswabidikmisi()->result();
        foreach ($tabel_bidikmisi as $n) {
            $pdf->Ln();
            $pdf->Cell(0.7, 0.8, $no++, 1, 0);
            $pdf->Cell(2.6, 0.8, $n->no_pendaftaran, 1, 0);
            $pdf->Cell(2.9, 0.8, $n->nama_lengkap, 1, 0);
            $pdf->Cell(2.0, 0.8, $n->jk, 1, 0);
            $pdf->Cell(2.1, 0.8, $n->tanggal_lahir, 1, 0);
            $pdf->Cell(2.4, 0.8, $n->alamat, 1, 0);
            $pdf->Cell(2.1, 0.8, $n->jml_tanggungan, 1, 0);
            $pdf->Cell(1.7, 0.8, $n->penghasilan, 1, 0);
            $pdf->Cell(1.9, 0.8, $n->tahun_tamat, 1, 0);
            $pdf->Cell(3.6, 0.8, $n->asal_sekolah, 1, 0);
            $pdf->Cell(1.7, 0.8, $n->rata2nilai, 1, 0);
            $pdf->Cell(4.4, 0.8, $n->prodi1, 1, 0);
        }
        $pdf->SetFont('Times', 'B', 12);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sp) {
            $pdf->MultiCell(23.5, 0.5, 'Diketahui', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(25, 0.5, $sp->nama_dekan, 0, 'R');
        }
        $pdf->Output();
    }
    public function laporan_seleksi_ppa()
    {
        include_once APPPATH . '/third_party/fpdf/fpdf.php';
        error_reporting(0);
        $pdf = new FPDF('L', 'cm', 'A4');
        $pdf->AddPage();
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Image('assets/images/logo_ummy.png', 1, 1, 2, 2); // pemanggilan logo
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN', 0, 'C');
        $pdf->SetFont('Times', 'B', 11);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Pendaftaran Beasiswa Online', 0, 'C');
        $pdf->SetFont('Times', 'B', 9);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Jl. Jend. Sudirman No.6, Kp.Jawa Kota Solok', 0, 'C');
        $pdf->SetFont('Times', 'B', 8);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Telp. (0755) 20565', 0, 'C');
        $pdf->Line(1, 3.1, 28.5, 3.1);
        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 3.2, 28.5, 3.2);
        $pdf->SetLineWidth(0);
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 12);
        $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $waktu[1] = date("d", time());
        $waktu[2] = date("m", time());
        $waktu[3] = date("Y", time());
        $tanggalini = "$waktu[1] " . $bulan[$waktu[2] - 1] . " $waktu[3]";
        $pdf->MultiCell(27, 0.5, ' Solok, ' . $tanggalini, 0, 'R');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 14);
        $pdf->MultiCell(27, 1.5, 'Laporan Seleksi Penerima Beasiswa PPA ', 0, 'C');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->setFillColor(222, 222, 222);
        $pdf->Cell(1.0, 0.8, 'No', 1, 0);
        $pdf->Cell(4.0, 0.8, 'NPM ', 1, 0);
        $pdf->Cell(4.8, 0.8, 'Nama Mahasiswa ', 1, 0);
        $pdf->Cell(3.2, 0.8, 'Jenis Kelamin', 1, 0);
        $pdf->Cell(6.5, 0.8, 'Jurusan', 1, 0);
        $pdf->Cell(3.5, 0.8, 'Ta.Akademik', 1, 0);
        $pdf->Cell(2.0, 0.8, 'IPK', 1, 0);
        $pdf->Cell(2.0, 0.8, 'Status', 1, 0);
        $pdf->SetFont('Times', 'B', 12);
        $no = 1;
        $tabel_ppa = $this->beasiswa_data->seleksi_beasiswa_ppa()->result();
        foreach ($tabel_ppa as $m) {
            $ipkmax = $m->ipk;
            if ($ipkmax >= 3.00) {
                $status = "LULUS";
            } else {
                $status = "GAGAL";
            }

            $pdf->Ln();
            $pdf->Cell(1.0, 0.8, $no++, 1, 0);
            $pdf->Cell(4.0, 0.8, $m->npm, 1, 0);
            $pdf->Cell(4.8, 0.8, $m->nama_mahasiswa, 1, 0);
            $pdf->Cell(3.2, 0.8, $m->jenis_kelamin, 1, 0);
            $pdf->Cell(6.5, 0.8, $m->prodi, 1, 0);
            $pdf->Cell(3.5, 0.8, $m->tahun_akademik, 1, 0);
            $pdf->Cell(2.0, 0.8, $ipkmax, 1, 0);
            $pdf->Cell(2.0, 0.8, $status, 1, 0);
        }
        $pdf->SetFont('Times', 'B', 12);
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $pdf->Ln();
        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sp) {
            $pdf->MultiCell(23.5, 0.5, 'Diketahui', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(25, 0.5, $sp->nama_dekan, 0, 'R');
        }
        $pdf->Output();
    }

    public function laporan_seleksi_bidikmisi()
    {
        include_once APPPATH . '/third_party/fpdf/fpdf.php';
        error_reporting(0);
        $pdf = new FPDF('L', 'cm', 'a4');
        $pdf->AddPage();
        $pdf->SetFont('Times', 'B', 16);
        $pdf->Image('assets/images/logo_ummy.png', 1, 1, 2, 2); // pemanggilan logo
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN', 0, 'C');
        $pdf->SetFont('Times', 'B', 11);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Pendaftaran Beasiswa Online', 0, 'C');
        $pdf->SetFont('Times', 'B', 9);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Jl. Jend. Sudirman No.6, Kp.Jawa Kota Solok', 0, 'C');
        $pdf->SetFont('Times', 'B', 8);
        $pdf->SetX(3);
        $pdf->MultiCell(24.5, 0.5, 'Telp. (0755) 20565', 0, 'C');
        $pdf->Line(1, 3.1, 28.5, 3.1);
        $pdf->SetLineWidth(0.1);
        $pdf->Line(1, 3.2, 28.5, 3.2);
        $pdf->SetLineWidth(0);
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 12);
        $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
        $waktu[1] = date("d", time());
        $waktu[2] = date("m", time());
        $waktu[3] = date("Y", time());
        $tanggalini = "$waktu[1] " . $bulan[$waktu[2] - 1] . " $waktu[3]";
        $pdf->MultiCell(27, 0.5, ' Solok, ' . $tanggalini, 0, 'R');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Times', 'B', 14);
        $pdf->MultiCell(27, 1.5, 'Laporan Pendaftar Beasiswa Bidikmisi ', 0, 'C');

        $pdf->SetFont('Times', 'B', 10);
        $pdf->setFillColor(222, 222, 222);
        $pdf->Cell(0.7, 0.8, 'No', 1, 0);
        $pdf->Cell(3.0, 0.8, 'No.Pendaftaran ', 1, 0);
        $pdf->Cell(3.9, 0.8, 'Nama Lengkap ', 1, 0);
        $pdf->Cell(2.4, 0.8, 'Tahun Tamat', 1, 0);
        $pdf->Cell(4.3, 0.8, 'Asal Sekolah', 1, 0);
        $pdf->Cell(2.1, 0.8, 'N.Rata-rata', 1, 0);
        $pdf->Cell(2.5, 0.8, 'Ta.Akademik', 1, 0);
        $pdf->Cell(1.5, 0.8, 'SNR', 1, 0);
        $pdf->Cell(1.5, 0.8, 'STK', 1, 0);
        $pdf->Cell(1.5, 0.8, 'SPK', 1, 0);
        $pdf->Cell(1.5, 0.8, 'SA', 1, 0);
        $pdf->Cell(2.0, 0.8, 'Status', 1, 0);

        $pdf->SetFont('Times', 'B', 10);
        $no = 1;
        $tabel_bidikmisi = $this->beasiswa_data->beasiswabidikmisi()->result();
        foreach ($tabel_bidikmisi as $n) {
            $rata = $n->rata2nilai;

            if ($rata >= 85) {
                $skor_rata2 = 5;
            } else if ($rata >= 80) {
                $skor_rata2 = 4;
            } elseif ($rata >= 75) {
                $skor_rata2 = 3;
            } elseif ($rata >= 70) {
                $skor_rata2 = 2;
            } else {
                $skor_rata2 = 1;
            }

            $tanggungan = $n->jml_tanggungan;

            if ($tanggungan >= 5) {
                $skor_tanggungan = 1;
            } else if ($tanggungan >= 4) {
                $skor_tanggungan = 2;
            } else if ($tanggungan >= 3) {
                $skor_tanggungan = 3;
            } else if ($tanggungan >= 2) {
                $skor_tanggungan = 4;
            } else {
                $skor_tanggungan = 5;
            }

            $penghasilan = $n->penghasilan;
            if ($penghasilan >= 4000000) {
                $skor_penghasilan = 1;
            } else if ($penghasilan >= 3000000) {
                $skor_penghasilan = 2;
            } else if ($penghasilan >= 2000000) {
                $skor_penghasilan = 3;
            } else if ($penghasilan >= 1000000) {
                $skor_penghasilan = 4;
            } else {
                $skor_penghasilan = 5;
            }


            $nilai_skor = ($skor_rata2 + $skor_penghasilan + $skor_tanggungan);

            if ($nilai_skor >= 10) {
                $status = "LULUS";
            } else {
                $status = "GAGAL";
            }

            $pdf->Ln();
            $pdf->Cell(0.7, 0.8, $no++, 1, 0);
            $pdf->Cell(3.0, 0.8, $n->no_pendaftaran, 1, 0);
            $pdf->Cell(3.9, 0.8, $n->nama_lengkap, 1, 0);
            $pdf->Cell(2.4, 0.8, $n->tahun_tamat, 1, 0);
            $pdf->Cell(4.3, 0.8, $n->asal_sekolah, 1, 0);
            $pdf->Cell(2.1, 0.8, $n->rata2nilai, 1, 0);
            $pdf->Cell(2.5, 0.8, $n->thn_akademik, 1, 0);
            $pdf->Cell(1.5, 0.8, $skor_rata2, 1, 0);
            $pdf->Cell(1.5, 0.8, $skor_tanggungan, 1, 0);
            $pdf->Cell(1.5, 0.8, $skor_penghasilan, 1, 0);
            $pdf->Cell(1.5, 0.8, $nilai_skor, 1, 0);
            $pdf->Cell(2.0, 0.8, $status, 1, 0);
        }
        $pdf->SetFont('Times', 'B', 12);
        $pdf->Ln();
        $pdf->Ln();
        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sr) {
            $pdf->MultiCell(23.5, 0.5, 'Keterangan :', 0, 'L');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(19.5, 0.5, '1. SNR adalah Skor Nilai Rata-rata ', 0, 'L');
            $pdf->MultiCell(19.5, 0.5, '2. STK adalah Skor Tunjangan Keluarga ', 0, 'L');
            $pdf->MultiCell(19.5, 0.5, '3. SPK adalah Skor Penghasilan Keluarga', 0, 'L');
            $pdf->MultiCell(19.5, 0.5, '4. SA adalah Skor Akhir ', 0, 'L');
            $pdf->Ln();
            $pdf->Ln();
            $pdf->Ln();

            $pdf->MultiCell(23.5, 0.5, 'Diketahui', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->MultiCell(19.5, 0.5, ' ', 0, 'R');
            $pdf->SetFont('Times', 'B', 12);
            $pdf->MultiCell(25, 0.5, $sr->nama_dekan, 0, 'R');
        }
        $pdf->Output();
    }

    public function excel_ppa()
    {
        include APPPATH . 'third_party/PHPExcel//PHPExcel.php';
        $excel = new PHPExcel();
        $excel->getProperties()->setCreator('MUHAMMAD RAFQI')
            ->setLastModifiedBy('MUHAMMAD RAFQI')
            ->setTitle("PENDAFTAR BEASISWA PPA")
            ->setSubject("PENDAFTAR BEASISWA")
            ->setDescription("PENDAFTAR BEASISWA PPA")
            ->setKeywords("PENDAFTAR BEASISWA PPA");

        $style_col = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );

        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );



        $excel->setActiveSheetIndex(0)->setCellValue('C1', "DATA PENDAFTAR BEASISWA PPA");
        $excel->getActiveSheet()->mergeCells('C1:G1');
        $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(TRUE);
        $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        $excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NAMA MAHASISWA");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "NPM");
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "JENIS KELAMIN");
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "JURUSAN");
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "IPK");
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "SEMESTER");
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "TA.AKADEMIK");

        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);

        $tabel_ppa = $this->beasiswa_data->beasiswappa()->result();
        $no = 1;
        $numrow = 4;
        foreach ($tabel_ppa as $data) {
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->nama_mahasiswa);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->npm);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->jenis_kelamin);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->prodi);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->ipk);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->semester);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->tahun_akademik);


            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);

            $no++;
            $numrow++;
        }

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);


        $excel->setActiveSheetIndex(0)->setCellValue('G22', "Diketahui Oleh");
        $excel->getActiveSheet()->mergeCells('G22:H22');
        $excel->getActiveSheet()->getStyle('G22')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('G22')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('G22')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('G23', "DEKAN,");
        $excel->getActiveSheet()->mergeCells('G23:H23');
        $excel->getActiveSheet()->getStyle('G23')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('G23')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('G23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sr) {
            $excel->setActiveSheetIndex(0)->setCellValue('G28', $sr->nama_dekan);
            $excel->getActiveSheet()->mergeCells('G28:H28');
            $excel->getActiveSheet()->getStyle('G28')->getFont()->setBold(FALSE);
            $excel->getActiveSheet()->getStyle('G28')->getFont()->setSize(12);
            $excel->getActiveSheet()->getStyle('G28')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }


        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $excel->getActiveSheet(0)->setTitle("L.PendaftarBeasiswaPPA");
        $excel->setActiveSheetIndex(0);
        $filename = "Pendaftar_beasiswa_ppa.xlsx";
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        ob_end_clean();
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function excel_bidikmisi()
    {
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';


        $excel = new PHPExcel();
        $excel->getProperties()->setCreator('MUHAMMAD RAFQI')
            ->setLastModifiedBy('MUHAMMAD RAFQI')
            ->setTitle("PENDAFTAR BEASISWA BIDIKMISI")
            ->setSubject("PENDAFTAR BEASISWA")
            ->setDescription("PENDAFTAR BEASISWA BIDIKMISI")
            ->setKeywords("PENDAFTAR BEASISWA BIDIKMISI");

        $style_col = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );

        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );



        $excel->setActiveSheetIndex(0)->setCellValue('F1', "DATA PENDAFTAR BEASISWA BIDIKMISI");
        $excel->getActiveSheet()->mergeCells('F1:L1');
        $excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(TRUE);
        $excel->getActiveSheet()->getStyle('F1')->getFont()->setSize(15);
        $excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NO.PENDAFTARAN");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "NAMA LENGKAP");
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "JENIS KELAMIN");
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "TANGGAL LAHIR");
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "ALAMAT");
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "NO.TELP");
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "PEKERJAAN AYAH");
        $excel->setActiveSheetIndex(0)->setCellValue('I3', "PEKERJAAN IBU");
        $excel->setActiveSheetIndex(0)->setCellValue('J3', "J.TANGGUNGAN");
        $excel->setActiveSheetIndex(0)->setCellValue('K3', "P.KELUARGA");
        $excel->setActiveSheetIndex(0)->setCellValue('L3', "T.LULUS");
        $excel->setActiveSheetIndex(0)->setCellValue('M3', "ASAL SEKOLAH");
        $excel->setActiveSheetIndex(0)->setCellValue('N3', "N.RATA-RATA");
        $excel->setActiveSheetIndex(0)->setCellValue('O3', "PRESTASI");
        $excel->setActiveSheetIndex(0)->setCellValue('P3', "PILIHAN 1");
        $excel->setActiveSheetIndex(0)->setCellValue('Q3', "PILIHAN 2");
        $excel->setActiveSheetIndex(0)->setCellValue('R3', "TA.AKADEMIK");

        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('M3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('N3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('O3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('P3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('Q3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('R3')->applyFromArray($style_col);

        $tabel_bidikmisi = $this->beasiswa_data->beasiswabidikmisi()->result();
        $no = 1;
        $numrow = 4;
        foreach ($tabel_bidikmisi as $data) {
            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->no_pendaftaran);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->nama_lengkap);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->jk);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->tanggal_lahir);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->alamat);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->no_telp);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $data->pekerjaan_ayah);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $data->pekerjaan_ibu);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $data->jml_tanggungan);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->penghasilan);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $data->tahun_tamat);
            $excel->setActiveSheetIndex(0)->setCellValue('M' . $numrow, $data->asal_sekolah);
            $excel->setActiveSheetIndex(0)->setCellValue('N' . $numrow, $data->rata2nilai);
            $excel->setActiveSheetIndex(0)->setCellValue('O' . $numrow, $data->prestasi);
            $excel->setActiveSheetIndex(0)->setCellValue('P' . $numrow, $data->prodi1);
            $excel->setActiveSheetIndex(0)->setCellValue('Q' . $numrow, $data->prodi2);
            $excel->setActiveSheetIndex(0)->setCellValue('R' . $numrow, $data->thn_akademik);


            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('M' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('N' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('O' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('P' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('Q' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('R' . $numrow)->applyFromArray($style_row);

            $no++;
            $numrow++;
        }

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('M')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('N')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('O')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('P')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('Q')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('R')->setWidth(15);

        $excel->setActiveSheetIndex(0)->setCellValue('Q11', "Diketahui Oleh");
        $excel->getActiveSheet()->mergeCells('Q11:R11');
        $excel->getActiveSheet()->getStyle('Q11')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('Q11')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('Q11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('Q12', "DEKAN,");
        $excel->getActiveSheet()->mergeCells('Q12:R12');
        $excel->getActiveSheet()->getStyle('Q12')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('Q12')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('Q12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sr) {
            $excel->setActiveSheetIndex(0)->setCellValue('Q17', $sr->nama_dekan);
            $excel->getActiveSheet()->mergeCells('Q17:R17');
            $excel->getActiveSheet()->getStyle('Q17')->getFont()->setBold(FALSE);
            $excel->getActiveSheet()->getStyle('Q17')->getFont()->setSize(12);
            $excel->getActiveSheet()->getStyle('Q17')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }


        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $excel->getActiveSheet(0)->setTitle("L.PendaftarBeasiswaBidikmisi");
        $excel->setActiveSheetIndex(0);

        $filename = "Pendaftar_beasiswa_bidikmisi.xlsx";
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        ob_end_clean();
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function excel_seleksippa()
    {
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';


        $excel = new PHPExcel();
        $excel->getProperties()->setCreator('MUHAMMAD RAFQI')
            ->setLastModifiedBy('MUHAMMAD RAFQI')
            ->setTitle("SELEKSI BEASISWA PPA")
            ->setSubject("SELEKSI BEASISWA")
            ->setDescription("SELEKSI BEASISWA PPA")
            ->setKeywords("SELEKSI BEASISWA PPA");

        $style_col = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );

        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );



        $excel->setActiveSheetIndex(0)->setCellValue('C1', "DATA SELEKSI BEASISWA PPA");
        $excel->getActiveSheet()->mergeCells('C1:G1');
        $excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(TRUE);
        $excel->getActiveSheet()->getStyle('C1')->getFont()->setSize(15);
        $excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NAMA MAHASISWA");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "NPM");
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "JENIS KELAMIN");
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "JURUSAN");
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "IPK");
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "TA.AKADEMIK");
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "STATUS");

        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);

        $tabel_ppa = $this->beasiswa_data->seleksi_beasiswa_ppa()->result();
        $no = 1;
        $numrow = 4;
        foreach ($tabel_ppa as $data) {
            $ipkmax = $data->ipk;
            if ($ipkmax >= 3.20) {
                $status = "LULUS";
            } else {
                $status = "GAGAL";
            }

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->nama_mahasiswa);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->npm);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->jenis_kelamin);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->prodi);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $ipkmax);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $data->tahun_akademik);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $status);


            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);

            $no++;
            $numrow++;
        }

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);

        $excel->setActiveSheetIndex(0)->setCellValue('G22', "Diketahui Oleh");
        $excel->getActiveSheet()->mergeCells('G22:H22');
        $excel->getActiveSheet()->getStyle('G22')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('G22')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('G22')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('G23', "DEKAN,");
        $excel->getActiveSheet()->mergeCells('G23:H23');
        $excel->getActiveSheet()->getStyle('G23')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('G23')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('G23')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sr) {
            $excel->setActiveSheetIndex(0)->setCellValue('G29', $sr->nama_dekan);
            $excel->getActiveSheet()->mergeCells('G29:H29');
            $excel->getActiveSheet()->getStyle('G29')->getFont()->setBold(FALSE);
            $excel->getActiveSheet()->getStyle('G29')->getFont()->setSize(12);
            $excel->getActiveSheet()->getStyle('G29')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $excel->getActiveSheet(0)->setTitle("L.SeleksiBeasiswaPPA");
        $excel->setActiveSheetIndex(0);

        $filename = "seleksi_beasiswa_ppa.xlsx";
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        ob_end_clean();
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }

    public function excel_seleksibidikmisi()
    {
        include APPPATH . 'third_party/PHPExcel/PHPExcel.php';


        $excel = new PHPExcel();
        $excel->getProperties()->setCreator('MUHAMMAD RAFQI')
            ->setLastModifiedBy('MUHAMMAD RAFQI')
            ->setTitle("SELEKSI BEASISWA BIDIKMISI")
            ->setSubject("SELEKSI BEASISWA")
            ->setDescription("SELEKSI BEASISWA BIDIKMISI")
            ->setKeywords("SELEKSI BEASISWA BIDIKMISI");

        $style_col = array(
            'font' => array('bold' => true),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );

        $style_row = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ),
            'borders' => array(
                'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
                'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN)
            )
        );



        $excel->setActiveSheetIndex(0)->setCellValue('D1', "DATA SELEKSI BEASISWA BIDIKMISI");
        $excel->getActiveSheet()->mergeCells('D1:H1');
        $excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(TRUE);
        $excel->getActiveSheet()->getStyle('D1')->getFont()->setSize(15);
        $excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('A3', "NO");
        $excel->setActiveSheetIndex(0)->setCellValue('B3', "NO.PENDAFTARAN");
        $excel->setActiveSheetIndex(0)->setCellValue('C3', "NAMA LENGKAP");
        $excel->setActiveSheetIndex(0)->setCellValue('D3', "ASAL SEKOLAH");
        $excel->setActiveSheetIndex(0)->setCellValue('E3', "TAHUN TAMAT");
        $excel->setActiveSheetIndex(0)->setCellValue('F3', "N.RATA2");
        $excel->setActiveSheetIndex(0)->setCellValue('G3', "SNR");
        $excel->setActiveSheetIndex(0)->setCellValue('H3', "STK");
        $excel->setActiveSheetIndex(0)->setCellValue('I3', "SPK");
        $excel->setActiveSheetIndex(0)->setCellValue('J3', "SA");
        $excel->setActiveSheetIndex(0)->setCellValue('K3', "TA.AKADEMIK");
        $excel->setActiveSheetIndex(0)->setCellValue('L3', "STATUS");

        $excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('I3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('J3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('K3')->applyFromArray($style_col);
        $excel->getActiveSheet()->getStyle('L3')->applyFromArray($style_col);

        $tabel_bidikmisi = $this->beasiswa_data->seleksi_beasiswa_bidikmisi()->result();
        $no = 1;
        $numrow = 4;
        foreach ($tabel_bidikmisi as $data) {
            $rata = $data->rata2nilai;

            if ($rata >= 85) {
                $skor_rata2 = 5;
            } else if ($rata >= 80) {
                $skor_rata2 = 4;
            } elseif ($rata >= 75) {
                $skor_rata2 = 3;
            } elseif ($rata >= 70) {
                $skor_rata2 = 2;
            } else {
                $skor_rata2 = 1;
            }

            $tanggungan = $data->jml_tanggungan;

            if ($tanggungan >= 5) {
                $skor_tanggungan = 1;
            } else if ($tanggungan >= 4) {
                $skor_tanggungan = 2;
            } else if ($tanggungan >= 3) {
                $skor_tanggungan = 3;
            } else if ($tanggungan >= 2) {
                $skor_tanggungan = 4;
            } else {
                $skor_tanggungan = 5;
            }

            $penghasilan = $data->penghasilan;
            if ($penghasilan >= 4000000) {
                $skor_penghasilan = 1;
            } else if ($penghasilan >= 3000000) {
                $skor_penghasilan = 2;
            } else if ($penghasilan >= 2000000) {
                $skor_penghasilan = 3;
            } else if ($penghasilan >= 1000000) {
                $skor_penghasilan = 4;
            } else {
                $skor_penghasilan = 5;
            }


            $nilai_skor = ($skor_rata2 + $skor_penghasilan + $skor_tanggungan);

            if ($nilai_skor >= 10) {
                $status = "LULUS";
            } else {
                $status = "GAGAL";
            }

            $excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
            $excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, $data->no_pendaftaran);
            $excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data->nama_lengkap);
            $excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data->asal_sekolah);
            $excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, $data->tahun_tamat);
            $excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, $data->rata2nilai);
            $excel->setActiveSheetIndex(0)->setCellValue('G' . $numrow, $skor_rata2);
            $excel->setActiveSheetIndex(0)->setCellValue('H' . $numrow, $skor_tanggungan);
            $excel->setActiveSheetIndex(0)->setCellValue('I' . $numrow, $skor_penghasilan);
            $excel->setActiveSheetIndex(0)->setCellValue('J' . $numrow, $nilai_skor);
            $excel->setActiveSheetIndex(0)->setCellValue('K' . $numrow, $data->thn_akademik);
            $excel->setActiveSheetIndex(0)->setCellValue('L' . $numrow, $status);


            $excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('G' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('H' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('I' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('J' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('K' . $numrow)->applyFromArray($style_row);
            $excel->getActiveSheet()->getStyle('L' . $numrow)->applyFromArray($style_row);

            $no++;
            $numrow++;
        }

        $excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
        $excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
        $excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);


        $excel->setActiveSheetIndex(0)->setCellValue('K11', "Diketahui Oleh");
        $excel->getActiveSheet()->mergeCells('K11:L11');
        $excel->getActiveSheet()->getStyle('K11')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('K11')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('K11')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $excel->setActiveSheetIndex(0)->setCellValue('K12', "DEKAN,");
        $excel->getActiveSheet()->mergeCells('K12:L12');
        $excel->getActiveSheet()->getStyle('K12')->getFont()->setBold(FALSE);
        $excel->getActiveSheet()->getStyle('K12')->getFont()->setSize(12);
        $excel->getActiveSheet()->getStyle('K12')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        $tabel_dekan = $this->beasiswa_data->dekan()->result();
        foreach ($tabel_dekan as $sr) {
            $excel->setActiveSheetIndex(0)->setCellValue('K17', $sr->nama_dekan);
            $excel->getActiveSheet()->mergeCells('K17:L17');
            $excel->getActiveSheet()->getStyle('K17')->getFont()->setBold(FALSE);
            $excel->getActiveSheet()->getStyle('K17')->getFont()->setSize(12);
            $excel->getActiveSheet()->getStyle('K17')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        $excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);

        $excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

        $excel->getActiveSheet(0)->setTitle("L.SeleksiBeasiswaBidikmisi");
        $excel->setActiveSheetIndex(0);

        $filename = "seleksi_beasiswa_bidikmisi.xlsx";
        $objWriter = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
        ob_end_clean();
        header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename=' . $filename);
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');
    }
}
