<?php defined('BASEPATH') or exit('No direct script access allowed');
require_once 'functions.php';
/**
 * This is beasiswa Controller
 */
class Beasiswa extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('beasiswa_data');
		$this->load->database();
		$this->load->helper(array('form', 'url', 'download'));
	}


	function user()
	{

		$data['user'] = $this->db->get_where('tabel_admin', ['username' =>
		$this->session->userdata('username')])->row_array();

		$this->load->view('tes/mypage', $data);
	}

	public function login_ad()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		if ($this->form_validation->run() == false) {
			$this->load->view('login_ad/login');
		} else {
			$this->_login();
		}
	}

	public function _login()
	{
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->db->get_where('tabel_admin', ['username' => $username])->row_array();
		if ($user) {
			//jika user ada
			if ($user['is_aktif'] == 1) {
				//cek pass
				if (password_verify($password, $user['password'])) {
					$data = [
						'username' => $user['username'],
						'role_id' => $user['role_id']

					];
					$this->session->set_userdata($data);
					redirect('beasiswa/indexLog');
				} else {
					$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">Password Salah!</div>');
					redirect('beasiswa/login_ad');
				}
			}
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">Username Tidak Terdaftar!</div>');
			redirect('beasiswa/login_ad');
		}
	}

	function registrasi_bidikmisi()
	{
		$this->form_validation->set_rules('no_pendaftaran', 'Nomor Pendaftaran', 'required|trim');
		$this->form_validation->set_rules('nama_lengkap', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('jk', 'Jenis Kelamin', 'required|trim');
		$this->form_validation->set_rules('alamat', 'Alamat', 'required|trim');
		$this->form_validation->set_rules('no_telp', 'No. Telp', 'required|trim');
		$this->form_validation->set_rules('tanggal_lahir', 'Tanggal Lahir', 'required|trim');
		$this->form_validation->set_rules('pekerjaan_ayah', 'Pekerjaan Ayah', 'required|trim');
		$this->form_validation->set_rules('pekerjaan_ibu', 'Pekerjaan Ibu', 'required|trim');
		$this->form_validation->set_rules('penghasilan', 'Penghasilan', 'required|trim');
		$this->form_validation->set_rules('jml_tanggungan', 'Jumlah Tanggungan', 'required|trim');
		$this->form_validation->set_rules('asal_sekolah', 'Asal Sekolah', 'required|trim');
		$this->form_validation->set_rules('tahun_tamat', 'Lulusan Tahun', 'required|trim');
		$this->form_validation->set_rules('rata2nilai', 'Rata-rata Nilai', 'required|trim');
		$this->form_validation->set_rules('prestasi', 'Prestasi', 'required|trim');
		$this->form_validation->set_rules('prodi1', 'Jurusan (Utama)', 'required|trim');
		$this->form_validation->set_rules('prodi2', 'Jurusan', 'required|trim');
		$this->form_validation->set_rules('thn_akademik', 'Tahun Akademik', 'required|trim');

		$config['upload_path']  = 'assets/berkas/';
		$config['allowed_types'] = 'jpg|jpeg';
		$config['max_size'] = 300;
		$this->load->library('upload', $config);

		//surat kurang mampu
		if (!empty($_FILES['surat_keterangan_tdkmampu'])) {
			$this->upload->do_upload('surat_keterangan_tdkmampu');
			$data1 = $this->upload->data();
		}
		if (!empty($_FILES['foto_rumah'])) {
			$this->upload->do_upload('foto_rumah');
			$data2 = $this->upload->data();
		}

		if ($this->form_validation->run() == false) {
			$data['kodeunik'] = $this->beasiswa_data->kode();
			$this->load->view('registrasi/registrasi_bidikmisi', $data);
		} else {
			$data = [

				'no_pendaftaran' => $this->input->post('no_pendaftaran'),
				'nama_lengkap' => $this->input->post('nama_lengkap'),
				'jk' => $this->input->post('jk'),
				'tanggal_lahir' => $this->input->post('tanggal_lahir') . "-" . $this->input->post('bulan_lahir') . "-" . $this->input->post('tahun_lahir'),
				'no_telp' => $this->input->post('no_telp'),
				'alamat' => $this->input->post('alamat'),
				'pekerjaan_ayah' => $this->input->post('pekerjaan_ayah'),
				'pekerjaan_ibu' => $this->input->post('pekerjaan_ibu'),
				'penghasilan' => $this->input->post('penghasilan'),
				'jml_tanggungan' => $this->input->post('jml_tanggungan'),
				'asal_sekolah' => $this->input->post('asal_sekolah'),
				'tahun_tamat' => $this->input->post('tahun_tamat'),
				'rata2nilai' => $this->input->post('rata2nilai'),
				'prestasi' => $this->input->post('prestasi'),
				'prodi1' => $this->input->post('prodi1'),
				'prodi2' => $this->input->post('prodi2'),
				'thn_akademik' => $this->input->post('thn_akademik'),
				'surat_keterangan_tdkmampu' => $data1['file_name'],
				'foto_rumah' => $data2['file_name']
			];
			$this->db->insert('tabel_bidikmisi', $data);
			$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Anda Berhasil Mendaftar Penerimaan Beasiswa Bidikmisi !!</div>');
			redirect('beasiswa/index');
		}
	}

	function registrasi_ppa()
	{
		$config['upload_path']  = 'assets/berkas/';
		$config['allowed_types'] = 'jpg|jpeg';
		$config['max_size'] = 300;


		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('surat_aktif_mhs')) {

			$this->load->view('registrasi/registrasi_ppa');
		} else {
			$upload_data = $this->upload->data();
			$data = array(

				'nama_mahasiswa' => $this->input->post('nama_mahasiswa'),
				'npm' => $this->input->post('npm'),
				'prodi' => $this->input->post('prodi'),
				'ipk' => $this->input->post('ipk'),
				'semester' => $this->input->post('semester'),
				'tahun_akademik' => $this->input->post('tahun_akademik'),
				'jenis_kelamin' => $this->input->post('jenis_kelamin'),
				'surat_aktif_mhs' => $upload_data['file_name'],
			);
			$this->db->insert('tabel_ppa', $data);
			$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Anda Berhasil Mendaftar Penerimaan Beasiswa PPA !!</div>');
			redirect('beasiswa/index');
		}
	}

	function registrasi_akun()
	{
		$this->form_validation->set_rules('nama', 'Nama Lengkap', 'required|trim');
		$this->form_validation->set_rules('username', 'Username', 'required|trim');
		$this->form_validation->set_rules('password', 'password', 'required|trim');

		if ($this->form_validation->run() == false) {
			$this->load->view('login_ad/registrasi_akun');
		} else {
			$data = [

				'nama' => $this->input->post('nama'),
				'username' => $this->input->post('username'),
				'password' => password_hash(
					$this->input->post('password'),
					PASSWORD_DEFAULT
				),
				'role_id' => 1,
				'is_aktif' => 1
			];

			$this->db->insert('tabel_admin', $data);
			$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Anda Berhasil Mendaftar !</div>');
			redirect('beasiswa/login_ad');
		}
	}



	function index()
	{
		$this->load->view('halaman_ad/halaman');
	}

	function indexLog()
	{

		$data['jumppa'] = $this->beasiswa_data->count_ppa();
		$data['jumbidikmisi'] = $this->beasiswa_data->count_bidikmisi();
		$data['jumbeasiswalainlain'] = $this->beasiswa_data->count_beasiswalainlain();
		$data['jumseleksippa'] = $this->beasiswa_data->count_seleksi_ppa();
		$data['jumseleksibidikmisi'] = $this->beasiswa_data->count_seleksi_bidikmisi();
		$data['jumdekan'] = $this->beasiswa_data->count_dekan();
		$data['jumpengumuman'] = $this->beasiswa_data->count_pengumuman();


		$this->load->view('tes/mypage', $data);
	}


	function tabel_pengumuman()
	{

		$data['tabel_pengumuman'] = $this->beasiswa_data->pengumuman()->result();

		$this->load->view('tes/tabel_pengumuman', $data);
	}

	function form_pengumuman()
	{

		$this->load->view('tes/form_pengumuman');
	}


	function tabel_seleksi_bidikmisi()
	{

		$data['tabel_bidikmisi'] = $this->beasiswa_data->seleksi_beasiswa_bidikmisi()->result();
		$this->load->view('tes/tabel_seleksi_bidikmisi', $data);
	}

	function tabel_seleksi_ppa()
	{

		$data['tabel_ppa'] = $this->beasiswa_data->seleksi_beasiswa_ppa()->result();
		$this->load->view('tes/tabel_seleksi_ppa', $data);
	}

	function tabel_ppa()
	{
		$data['tabel_ppa'] = $this->beasiswa_data->beasiswappa()->result();
		$this->load->view('tes/tabel_ppa', $data);
	}
	function tabel_berkas()
	{
		$data['tabel_berkas'] = $this->beasiswa_data->berkasbidikmisi()->result();
		$this->load->view('tes/tabel_berkas', $data);
	}

	function tabel_filterppa($tahun_akademik = null)
	{
		$this->load->model('Beasiswa_data');
		$tahun_akademik = $this->input->get_post('tahun_akademik');
		$data = $this->beasiswa_data->filter_beasiswappa($tahun_akademik);
		$data = array(
			'tahun_akademik'	=> $tahun_akademik,
			'data'		=> $data
		);
		$this->load->view('tes/tabel_cari', $data);
	}


	function tabel_bidikmisi()
	{

		$data['tabel_bidikmisi'] = $this->beasiswa_data->beasiswabidikmisi()->result();
		$this->load->view('tes/tabel_bidikmisi', $data);
	}

	function tabel_beasiswalainlain()
	{

		$data['tabel_beasiswalainlain'] = $this->beasiswa_data->beasiswalainlain()->result();
		$this->load->view('tes/tabel_beasiswalainlain', $data);
	}

	function profil()
	{
		$data['tabel_admin'] = $this->beasiswa_data->profil()->result();
		$this->load->view('tes/profil', $data);
	}



	public function add_pengumuman()
	{
		$config['upload_path'] = 'application/uploads/';
		$config['allowed_types'] = 'doc|docx|pdf';
		$config['max_size'] = 0;


		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('name_file')) {


			$this->load->view('tes/form_pengumuman');
		} else {
			$upload_data = $this->upload->data();
			$data = array(
				'name_file' => $upload_data['file_name']
			);
			$this->db->insert('tabel_pengumuman', $data);
			redirect('beasiswa/tabel_pengumuman');
		}
	}

	function form_periode()
	{

		$this->load->view('tes/form_periode');
	}

	function add_periode()
	{
		$tgl_awal = date("Y-m-d", strtotime($this->input->post('tgl_awal')));
		$tgl_akhir = date("Y-m-d", strtotime($this->input->post('tgl_akhir')));


		$data = array(
			'tgl_awal' => $tgl_awal,
			'tgl_akhir' => $tgl_akhir

		);
		$this->beasiswa_data->insert_data($data, 'periode_beasiswa');

		$this->session->set_flashdata('periode_added', 'Periode berhasil ditambahkan');
		redirect('beasiswa/tabel_periode');
	}


	function download_pengumuman()
	{
		$data = $this->db->get_where('tabel_pengumuman')->row();
		force_download('application/uploads/' . $data->name_file, NULL);
	}

	function edit_form_pengumuman($id_pengumumnan)
	{
		$where = array('id_pengumuman' => $id_pengumumnan);
		$data['tabel_pengumuman'] = $this->beasiswa_data->edit_data($where, 'tabel_pengumuman')->result();
		$this->load->view('tes/edit_form_pengumuman', $data);
	}

	function update_pengumuman()
	{
		$id_pengumuman = $this->input->post('id_pengumuman');

		$config['upload_path']         = 'application/uploads/';  // foler upload 
		$config['allowed_types']        = 'doc|docx|pdf'; // jenis file
		$config['max_size']             = 0;
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('name_file')) //sesuai dengan name pada form 
		{
			$this->load->view('tes/form_pengumuman');
		} else {
			//tampung data dari form
			$upload_data = $this->upload->data();
			$name_file = $upload_data['file_name'];

			$data = array(
				'name_file' => $name_file,

			);
			$where = array(
				'id_pengumuman' => $id_pengumuman,
			);
			$this->beasiswa_data->update_data($where, $data, 'tabel_pengumuman');
			$this->session->set_flashdata('pengumuman_added', 'File Pengumuman berhasil diperbarui');
			redirect('beasiswa/tabel_pengumuman');
		}
	}

	function edit_form_ppa($id_ppa)
	{
		$where = array('id_ppa' => $id_ppa);
		$data['tabel_ppa'] = $this->beasiswa_data->edit_data($where, 'tabel_ppa')->result();
		$this->load->view('tes/edit_form_ppa', $data);
	}

	function update_kriteria()
	{
		$id_kriteria = $this->input->post('id_kriteria');
		$nama_kriteria = $this->input->post('nama_kriteria');


		$data = array(
			'nama_kriteria' => $nama_kriteria


		);

		$where = array(
			'id_kriteria' => $id_kriteria
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_kriteria');
		$this->session->set_flashdata('kriteria_added', 'Data Kriteria berhasil diperbarui');
		redirect('beasiswa/tabel_kriteria');
	}

	function update_detail_kriteria()
	{
		$id_detail_kriteria = $this->input->post('id_detail_kriteria');
		$nama_detail_kriteria = $this->input->post('nama_detail_kriteria');
		$bobot_detail_kriteria = $this->input->post('bobot_detail_kriteria');



		$data = array(
			'nama_detail_kriteria' => $nama_detail_kriteria,
			'bobot_detail_kriteria' => $bobot_detail_kriteria


		);

		$where = array(
			'id_detail_kriteria' => $id_detail_kriteria
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_detail_kriteria');
		$this->session->set_flashdata('detail_kriteria_added', 'Data Detail Kriteria berhasil diperbarui');
		redirect('beasiswa/tabel_detail_kriteria');
	}

	function update_ppa()
	{
		$id_ppa = $this->input->post('id_ppa');
		$nama_mahasiswa = $this->input->post('nama_mahasiswa');
		$npm = $this->input->post('npm');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$prodi = $this->input->post('prodi');
		$ipk = $this->input->post('ipk');
		$semester = $this->input->post('semester');
		$tahun_akademik = $this->input->post('tahun_akademik');


		$data = array(
			'nama_mahasiswa' => $nama_mahasiswa,
			'npm' => $npm,
			'jenis_kelamin' => $jenis_kelamin,
			'prodi' => $prodi,
			'ipk' => $ipk,
			'semester' => $semester,
			'tahun_akademik' => $tahun_akademik

		);

		$where = array(
			'id_ppa' => $id_ppa
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_ppa');
		$this->session->set_flashdata('ppa_added', 'Data PPA berhasil diperbarui');
		redirect('beasiswa/tabel_ppa');
	}

	function update_profil()
	{
		$id_user = $this->input->post('id_user');
		$nrp = $this->input->post('nrp');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
		$no_telp = $this->input->post('no_telp');
		$email = $this->input->post('email');
		$alamat = $this->input->post('alamat');


		$data = array(
			'nrp' => $nrp,
			'nama' => $nama,
			'username' => $username,
			'password' => $password,
			'no_telp' => $no_telp,
			'email' => $email,
			'alamat' => $alamat

		);

		$where = array(
			'id_user' => $id_user
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_admin');
		$this->session->set_flashdata('profil_added', 'Profil berhasil diperbarui!');
		redirect('beasiswa/profil');
	}

	function edit_form_profil($id_user)
	{
		$where = array('id_user' => $id_user);
		$data['tabel_admin'] = $this->beasiswa_data->edit_data($where, 'tabel_admin')->result();

		$this->load->view('tes/edit_form_profil', $data);
	}

	function edit_form_kriteria($id_kriteria)
	{
		$where = array('id_kriteria' => $id_kriteria);
		$data['tabel_kriteria'] = $this->beasiswa_data->edit_data($where, 'tabel_kriteria')->result();

		$this->load->view('tes/edit_form_kriteria', $data);
	}

	function edit_form_detail_kriteria($id_detail_kriteria)
	{
		$where = array('id_detail_kriteria' => $id_detail_kriteria);
		$data['tabel_detail_kriteria'] = $this->beasiswa_data->edit_data($where, 'tabel_detail_kriteria')->result();

		$this->load->view('tes/edit_form_detail_kriteria', $data);
	}

	function edit_form_bidikmisi($id_bidikmisi)
	{
		$where = array('id_bidikmisi' => $id_bidikmisi);
		$data['tabel_bidikmisi'] = $this->beasiswa_data->edit_data($where, 'tabel_bidikmisi')->result();

		$this->load->view('tes/edit_form_bidikmisi', $data);
	}


	function edit_form_lainlain($id_beasiswalain)
	{
		$where = array('id_beasiswalain' => $id_beasiswalain);
		$data['tabel_beasiswalainlain'] = $this->beasiswa_data->edit_data($where, 'tabel_beasiswalainlain')->result();
		$this->load->view('tes/edit_form_lainlain', $data);
	}


	function update_beasiswalain()
	{
		$id_beasiswalain = $this->input->post('id_beasiswalain');
		$nama_mahasiswa1 = $this->input->post('nama_mahasiswa1');
		$jenis_beasiswa = $this->input->post('jenis_beasiswa');
		$prodi1 = $this->input->post('prodi1');

		$data = array(
			'nama_mahasiswa1' => $nama_mahasiswa1,
			'jenis_beasiswa' => $jenis_beasiswa,
			'prodi1' => $prodi1,
		);

		$where = array(
			'id_beasiswalain' => $id_beasiswalain
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_beasiswalainlain');

		$this->session->set_flashdata('beasiswalain_added', 'Data berhasil diperbarui');
		redirect('beasiswa/tabel_beasiswalainlain');
	}

	function update_bidikmisi()
	{
		$id_bidikmisi = $this->input->post('id_bidikmisi');
		$nama_lengkap = $this->input->post('nama_lengkap');
		$jk = $this->input->post('jk');
		$tanggal_lahir = date("Y-m-d", strtotime($this->input->post('tanggal_lahir')));
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$pekerjaan_ayah = $this->input->post('pekerjaan_ayah');
		$pekerjaan_ibu = $this->input->post('pekerjaan_ibu');
		$jml_tanggungan = $this->input->post('jml_tanggungan');
		$penghasilan = $this->input->post('penghasilan');
		$asal_sekolah = $this->input->post('asal_sekolah');
		$tahun_tamat = $this->input->post('tahun_tamat');
		$rata2nilai = $this->input->post('rata2nilai');
		$prestasi = $this->input->post('prestasi');
		$prodi1 = $this->input->post('prodi1');
		$prodi2 = $this->input->post('prodi2');
		$thn_akademik = $this->input->post('thn_akademik');

		$data = array(
			'nama_lengkap' => $nama_lengkap,
			'jk' => $jk,
			'tanggal_lahir' => $tanggal_lahir,
			'no_telp' => $no_telp,
			'alamat' => $alamat,
			'pekerjaan_ayah' => $pekerjaan_ayah,
			'pekerjaan_ibu' => $pekerjaan_ibu,
			'jml_tanggungan' => $jml_tanggungan,
			'penghasilan' => $penghasilan,
			'asal_sekolah' => $asal_sekolah,
			'tahun_tamat' => $tahun_tamat,
			'rata2nilai' => $rata2nilai,
			'prestasi' => $prestasi,
			'prodi1' => $prodi1,
			'prodi2' => $prodi2,
			'thn_akademik' => $thn_akademik

		);

		$where = array(
			'id_bidikmisi' => $id_bidikmisi
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_bidikmisi');
		$this->session->set_flashdata('bidikmisi_added', 'Data berhasil diperbarui');
		redirect('beasiswa/tabel_bidikmisi');
	}


	function remove_pengumuman($id_pengumuman)
	{
		$this->db->where('id_pengumuman', $id_pengumuman);
		$query = $this->db->get('tabel_pengumuman');
		$row = $query->row();

		unlink("application/uploads/$row->name_file");

		$this->db->delete('tabel_pengumuman', array('id_pengumuman' => $id_pengumuman));
		redirect('beasiswa/tabel_pengumuman');
	}

	function remove_periode($id_periode_beasiswa)
	{
		$where = array('id_periode_beasiswa' => $id_periode_beasiswa);
		$this->beasiswa_data->delete_data($where, 'periode_beasiswa');
		redirect('beasiswa/tabel_periode');
	}

	function remove_ppa($id_ppa)
	{
		$where = array('id_ppa' => $id_ppa);
		$this->beasiswa_data->delete_data($where, 'tabel_ppa');
		redirect('beasiswa/tabel_ppa');
	}

	function remove_seleksi_ppa($id_ppa)
	{
		$where = array('id_ppa' => $id_ppa);
		$this->beasiswa_data->delete_data($where, 'tabel_ppa');
		redirect('beasiswa/tabel_seleksi_ppa');
	}

	function remove_kategori($id_kategori)
	{
		$where = array('id_kategori' => $id_kategori);
		$this->beasiswa_data->delete_data($where, 'jenis_kategori');
		redirect('beasiswa/tabel_kategori');
	}

	function remove_bidikmisi($id_bidikmisi)
	{
		$where = array('id_bidikmisi' => $id_bidikmisi);
		$this->beasiswa_data->delete_data($where, 'tabel_bidikmisi');
		redirect('beasiswa/tabel_bidikmisi');
	}
	function remove_seleksi_bidikmisi($id_bidikmisi)
	{
		$where = array('id_bidikmisi' => $id_bidikmisi);
		$this->beasiswa_data->delete_data($where, 'tabel_bidikmisi');
		redirect('beasiswa/tabel_seleksi_bidikmisi');
	}

	function remove_beasiswalain($id_beasiswalain)
	{
		$where = array('id_beasiswalain' => $id_beasiswalain);
		$this->beasiswa_data->delete_data($where, 'tabel_beasiswalainlain');
		redirect('beasiswa/tabel_beasiswalainlain');
	}
	function logout()
	{
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('role_id');
		$this->session->set_flashdata('pesan', '<div class="alert alert-success" role="alert">Kamu Telah Keluar!</div>');
		redirect('beasiswa/login_ad');
	}

	function tabel_dekan()
	{

		$data['tabel_dekan'] = $this->beasiswa_data->dekan()->result();
		$this->load->view('tes/tabel_dekan', $data);
	}

	function form_dekan()
	{

		$this->load->view('tes/form_dekan');
	}

	function add_dekan()
	{
		$nip = $this->input->post('nip');
		$nama_dekan = $this->input->post('nama_dekan');

		$data = array(
			'nip' => $nip,
			'nama_dekan' => $nama_dekan

		);
		$this->beasiswa_data->insert_data($data, 'tabel_dekan');

		$this->session->set_flashdata('dekan_added', 'Dekan berhasil ditambahkan');
		redirect('beasiswa/tabel_dekan');
	}
	function update_dekan()
	{
		$id_dekan = $this->input->post('id_dekan');
		$nip = $this->input->post('nip');
		$nama_dekan = $this->input->post('nama_dekan');


		$data = array(
			'nip' => $nip,
			'nama_dekan' => $nama_dekan

		);

		$where = array(
			'id_dekan' => $id_dekan
		);

		$this->beasiswa_data->update_data($where, $data, 'tabel_dekan');
		$this->session->set_flashdata('dekan_added', 'Data berhasil diperbarui');
		redirect('beasiswa/tabel_dekan');
	}

	function edit_form_dekan($id_dekan)
	{
		$where = array('id_dekan' => $id_dekan);
		$data['tabel_dekan'] = $this->beasiswa_data->edit_data($where, 'tabel_dekan')->result();
		$this->load->view('tes/edit_form_dekan', $data);
	}

	function remove_dekan($id_dekan)
	{
		$where = array('id_dekan' => $id_dekan);
		$this->beasiswa_data->delete_data($where, 'tabel_dekan');
		redirect('beasiswa/tabel_dekan');
	}
}
