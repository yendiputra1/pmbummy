<?php

/**
 * This is Data Model
 */
class Beasiswa_data extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function beasiswappa()
    {

        $this->db->order_by('ipk', 'desc');
        return $this->db->get('tabel_ppa');
    }
    function filter_beasiswappa($tahun_akademik)
    {

        // $query = $this->db->query("SELECT * FROM tabel_ppa where tahun_akademik='$tahun_akademik' ");
        // return $query->result_array();

        $this->db->select('*');
        $this->db->from('tabel_ppa');
        $this->db->order_by('ipk', 'desc');
        if (!empty($tahun_akademik)) {
            $this->db->like('tahun_akademik', $tahun_akademik);
        }
        return $this->db->get()->result_array();
    }
    public function fpdf_beasiswappa($tahun_akademik)
    {

        $this->db->select('*');
        $this->db->from('tabel_ppa');
        $this->db->order_by('ipk', 'desc');
        if (!empty($tahun_akademik)) {
            $this->db->like('tahun_akademik', $tahun_akademik);
        }
        return $this->db->get()->result_array();
    }


    function beasiswabidikmisi()
    {
        $this->db->order_by('thn_akademik', 'desc');
        return $this->db->get('tabel_bidikmisi');
    }

    function berkasbidikmisi()
    {
        $this->db->order_by('no_pendaftaran', 'desc');
        return $this->db->get('tabel_bidikmisi');
    }


    function beasiswalainlain()
    {
        return $this->db->get('tabel_beasiswalainlain');
    }

    function pengumuman()
    {
        return $this->db->get('tabel_pengumuman');
    }

    function profil()
    {
        return $this->db->get('tabel_admin');
    }

    function dekan()
    {
        $this->db->select('*');
        $this->db->from('tabel_dekan');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query;
    }

    function kriteria()
    {
        return $this->db->get('tabel_kriteria');
    }

    function detail_kriteria()
    {
        $this->db->select('tabel_detail_kriteria.*,nama_kriteria');
        $this->db->from('tabel_detail_kriteria');
        $this->db->join('tabel_kriteria', 'tabel_kriteria.id_kriteria = tabel_detail_kriteria.id_kriteria');
        $query = $this->db->get();
        return $query;
    }

    function seleksi_beasiswa_bidikmisi()
    {
        $this->db->order_by('rata2nilai', 'desc');
        return $this->db->get('tabel_bidikmisi');
    }

    function seleksi_beasiswa_ppa()
    {
        $this->db->order_by('ipk', 'desc');
        return $this->db->get('tabel_ppa');
    }

    function periode_beasiswa()
    {
        return $this->db->get('periode_beasiswa');
    }



    function get_ppa()
    {
        $data = array();
        $query = $this->db->get('tabel_ppa')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nama_mahasiswa']] = $row['nama_mahasiswa'];
            }
        }
        asort($data);
        return $data;
    }



    function get_beasiswalainlain()
    {
        $data = array();
        $query = $this->db->get('tabel_beasiswalainlain')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nama_mahasiswa1']] = $row['nama_mahasiswa1'];
            }
        }
        asort($data);
        return $data;
    }


    function get_bidikmisi()
    {
        $data = array();
        $query = $this->db->get('tabel_bidikmisi')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nama_lengkap']] = $row['nama_lengkap'];
            }
        }
        asort($data);
        return $data;
    }

    function get_pengumuman()
    {
        $data = array();
        $query = $this->db->get('tabel_pengumuman')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nama_file']] = $row['nama_file'];
            }
        }
        asort($data);
        return $data;
    }

    function get_profil()
    {
        $data = array();
        $query = $this->db->get('tabel_admin')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nrp']] = $row['nrp'];
            }
        }
        asort($data);
        return $data;
    }

    function get_dekan()
    {
        $data = array();
        $query = $this->db->get('tabel_dekan')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nip']] = $row['nip'];
            }
        }
        asort($data);
        return $data;
    }

    function get_kriteria()
    {
        $data = array();
        $query = $this->db->get('tabel_kriteria')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['nama_kriteria']] = $row['nama_kriteria'];
            }
        }
        asort($data);
        return $data;
    }

    function get_periode_beasiswa()
    {
        $data = array();
        $query = $this->db->get('periode_beasiswa')->result_array();

        if (is_array($query) && count($query) > 0) {
            foreach ($query as $row) {
                $data[$row['tgl_awal']] = $row['tgl_awal'];
            }
        }
        asort($data);
        return $data;
    }


    function kode()
    {
        $this->db->select('RIGHT(tabel_bidikmisi.no_pendaftaran,2) as no_pendaftaran', false);
        $this->db->order_by('no_pendaftaran', 'DESC');
        $this->db->limit(1);
        $query = $this->db->get('tabel_bidikmisi');
        if ($query->num_rows() <> 0) {
            //cek kode jika tersedia
            $data = $query->row();
            $kode = intval($data->no_pendaftaran) + 1;
        } else {
            $kode = 1; //cek jika kode belum ada pada tabel
        }
        $tgl = date('dmY');
        $batas = str_pad($kode, 3, "0", STR_PAD_LEFT);
        $kodetampil = "2021-" . $tgl . $batas; //format kode
        return $kodetampil;
    }

    public function get($username)
    {
        $this->db->where('username', $username); // Untuk menambahkan Where Clause : username='$username'
        $result = $this->db->get('tabel_admin')->row(); // Untuk mengeksekusi dan mengambil data hasil query
        return $result;
    }

    function insert_data($data, $table)
    {
        $this->db->insert($table, $data);
    }

    function edit_data($where, $table)
    {
        return $this->db->get_where($table, $where);
    }

    function update_data($where, $data, $table)
    {
        $this->db->where($where);
        $this->db->update($table, $data);
    }

    function delete_data($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }



    function count_ppa()
    {
        $cp =  $this->db->query('SELECT * FROM tabel_ppa');
        $jumppa = $cp->num_rows();
        return $jumppa;
    }

    function count_bidikmisi()
    {
        $cb =  $this->db->query('SELECT * FROM tabel_bidikmisi');
        $jumbidikmisi = $cb->num_rows();
        return $jumbidikmisi;
    }

    function count_beasiswalainlain()
    {
        $cl =  $this->db->query('SELECT * FROM tabel_beasiswalainlain');
        $jumbeasiswalainlain = $cl->num_rows();
        return $jumbeasiswalainlain;
    }

    function count_pengumuman()
    {
        $ck =  $this->db->query('SELECT * FROM tabel_pengumuman');
        $jumpengumuman = $ck->num_rows();
        return $jumpengumuman;
    }
    function count_seleksi_ppa()
    {
        $sk =  $this->db->query('SELECT * FROM tabel_ppa');
        $jumseleksippa = $sk->num_rows();
        return $jumseleksippa;
    }
    function count_seleksi_bidikmisi()
    {
        $sl =  $this->db->query('SELECT * FROM tabel_bidikmisi');
        $jumseleksibidikmisi = $sl->num_rows();
        return $jumseleksibidikmisi;
    }
    function count_dekan()
    {
        $sr =  $this->db->query('SELECT * FROM tabel_dekan');
        $jumdekan = $sr->num_rows();
        return $jumdekan;
    }
}
