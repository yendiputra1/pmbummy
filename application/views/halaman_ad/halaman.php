<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Universitas Mahaputra Muhammad Yamin </title>
    <base href="<?php echo base_url(); ?>" />
    <link rel="shortcut icon" href="assets/images/ummy.ico" type="image/png">
    <!-- Bootstrap Core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css2/faa.css" rel="stylesheet">
    <!-- Theme CSS -->
    <link href="assets/css2/freelancer.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="assets/vendor/jquery/jquery.min.js"></script>

    <!-- Custom Fonts -->
    <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>


<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" style="background-color: #ba1919;" class=" navbar navbar-default navbar-fixed-top navbar-custom bxshad">
        <div class="container">
            <?php echo $this->session->flashdata('pesan'); ?>
            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="#page-top">
                    <img src="assets/images/logo_ummy.png" alt="Logo" width="35" style="position:absolute;margin-top:-10px;">
                    <span style="margin-left:45px; ">&nbsp;PENDAFTARAN BEASISWA </span>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li class="page-scroll">
                        <a href="#daftar"><i class="fa fa-book"></i> Pendaftaran</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#about"><i class="fa fa-info-circle"></i> Informasi</a>
                    </li>
                    <li class="page-scroll">
                        <a href="#syarat"><i class="fa fa-folder"></i> Persyaratan</a>
                    </li>
                    <!-- <li class="page-scroll">
                        <a href="<?php echo base_url('beasiswa/login_ad') ?>"><i class="fa fa-sign-in"></i> Login</a>
                    </li> -->

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>
    <!-- Header -->
    <header>
        <section id="daftar">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <img class="img-responsive" src="assets/images/logo_ummy.png" style="margin-top:-15%;margin-bottom:-10px;" width="100">
                        <br><br>
                        <form enctype="multipart/form-data">
                            <div class="intro-text">
                                <span class="name shad" style="font-size:35px; line-height: 35px;">
                                    SELAMAT DATANG DI PENDAFTARAN BEASISWA<br> UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN SOLOK
                                </span>
                                <br>
                                <span>
                                    <a href="<?php echo base_url('beasiswa/registrasi_bidikmisi') ?>" class="btn btn-primary" style="margin: 5px; border-radius: 6px;">
                                        <i class="fa fa-list faa-pulse"></i> &nbsp;
                                        <b>DAFTAR BIDIK MISI</b>
                                    </a>
                                    <a href="<?php echo base_url('beasiswa/registrasi_ppa') ?>" class="btn btn-primary" style="margin: 5px; border-radius: 6px;">
                                        <i class="fa fa-list faa-pulse"></i> &nbsp;
                                        <b>DAFTAR PPA</b>
                                    </a>
                                    <br>


                                </span>
                                <span class="skills">
                                </span>
                                <br>

                                <span>
                                    <a href="<?php echo base_url() . 'beasiswa/download_pengumuman' ?>" class="btn btn-danger btn-lg animated" style="margin:5px; border-radius: 6px;"><i class="fa fa-file faa-pulse "></i> &nbsp;<b>PENGUMUMAN CEK DISINI!!</b></a>
                                    <br>
                                </span>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </header>
    <style>
        table {
            margin: auto;
            font-family: "Lucida Sans Unicode", "Lucida Grande", "Segoe Ui";
            font-size: 12px;

        }

        .demo-table {
            border-collapse: collapse;
            font-size: 13px;
        }

        .demo-table th,
        .demo-table td {
            border-bottom: 1px solid #e1edff;
            border-left: 1px solid #e1edff;
            padding: 7px 17px;
        }

        .demo-table th,
        .demo-table td:last-child {
            border-right: 1px solid #e1edff;
        }

        .demo-table td:first-child {
            border-top: 1px solid #e1edff;
        }

        .demo-table td:last-child {
            border-bottom: 0;
        }

        caption {
            caption-side: top;
            margin-bottom: 10px;
        }

        /* Table Header */
        .demo-table thead th {
            background-color: #508abb;
            color: #FFFFFF;
            border-color: #6ea1cc !important;
            text-transform: uppercase;
        }

        /* Table Body */
        .demo-table tbody td {
            color: #353535;
        }

        .demo-table tbody tr:nth-child(odd) td {
            background-color: #f4fbff;
        }

        .demo-table tbody tr:hover th,
        .demo-table tbody tr:hover td {
            background-color: #ffffa2;
            border-color: #ffff0f;
            transition: all .2s;
        }
    </style>

    <!-- About Section -->

    <section class="success" id="about" style="background-color: #ba1919; padding: 30px; border-top: 2px solid #fff;">
        <div class="container">
            <div class=" row">
                <div class="col-lg-12 text-center">
                    <h2>Informasi PENDAFTARAN BEASISWA Online</h2>
                    <hr style="width: 150px;">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-2" style="text-align:justify; line-height: 22px;">
                    <p>UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN menyediakan PENDAFTARAN BEASISWA secara <i>online</i> diharapkan proses PENDAFTARAN dapat berjalan dengan cepat
                        dan bisa dilakukan dimanapun dan kapanpun selama sesi PENDAFTARAN dibuka. Proses pendaftaran calon penerima beasiswa di masa pandemi Covid-19 ini dan terhambat oleh jarak, waktu, kondisi jika datang ke kampus secara langsung, dengan adanya website PENDAFTARAN BEASISWA <i>online</i> di UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN dapat mempermudah waktu dan juga biaya. </p>
                </div>
                <div class="col-lg-4" style="text-align:justify; line-height: 22px;">
                    <p>Jenis beasiswa yang ada pada Universitas Mahaputra Muhammad Yamin Kota Solok yaitu Bidik Misi dan PPA. Bidik Misi adalah bantuan biaya pendidikan bagi calon mahasiswa yang tidak mampu secara ekonomi dan memliki potensi akademik yang baik untuk menempuh pendidikan di perguruan tinggi pada program studi unggulan sampai lulus tepat waktu. Sedangkan PPA adalah bantuan kuliah dari Kementrian Ristek Dikti yang diperlukan bagi mahasiswa aktif jenjang S1 dan D3 yang telah memasuki Semester II. </p>
                </div>
            </div>
        </div>
    </section>


    <section id="syarat" style="background: url(img/bg.png) repeat; padding: 30px;">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Syarat Pendaftaran</h2>
                    <hr style="width: 150px;">

                </div>
            </div>
            <div class="row">
                <div class="col-lg-12" style="margin-top:-10px;">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">
                        <img class="img-responsive" src="assets/images/syarat1.png" alt="">
                        <br>
                        <img class="img-responsive" src="assets/images/syarat2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer -->
    <footer class="text-center">
        <div class="footer-below" style="background-color: #ba1919;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        PENDAFTARAN BEASISWA &copy; UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN - <?php echo date('Y'); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-top page-scroll hidden-sm hidden-xs hidden-lg hidden-md">
        <a class="btn btn-primary" href="#page-top">
            <i class="fa fa-chevron-up"></i>
        </a>
    </div>


    <!-- Bootstrap Core JavaScript -->
    <script src="assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="assets/js2/jqBootstrapValidation.js"></script>
    <script src="assets/js2/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="assets/js2/freelancer.min.js"></script>
    <script>
        $(function() {
            $("#example1").DataTable();
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
</body>

</html>