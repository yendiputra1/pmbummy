<div class="col-md-3 left_col menu_fixed">
	<div class="left_col scroll-view">
		<div class="navbar nav_title" style="border: 0;">
			<!-- logo info -->
			<div class="logo_pic">
				<a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('assets/images/logo_ummy.png') ?>" alt="..." class="img-circle logo_img"></a>
			</div>

		</div>
		<div class="profile">
			<a href="<?php echo base_url(); ?>" class="site_title"><span style="font-size: 20px;"><?php echo 'Univesitas Mahaputra Muhammad Yamin' ?></span></a>
		</div>


		<div class="clearfix"></div>


		<!-- /menu profile quick info -->
		<br>
		<!-- Sidebar Menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3></h3>
				<ul class="nav side-menu">

					<li><a href="<?php echo base_url('') ?>"><i class="fa fa-home"></i> Dashboard </a></li>
					<li><a><i class="fa fa-group"></i> Data Mahasiswa <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('beasiswa/tabel_ppa') ?>">Beasiswa PPA</a></li>
							<li><a href="<?php echo base_url('beasiswa/tabel_bidikmisi') ?>">Beasiswa Bidik Misi</a></li>
							<li><a href="<?php echo base_url('beasiswa/tabel_beasiswalainlain') ?>">Beasiswa Lain-Lain</a></li>

						</ul>
					</li>
					<li><a><i class="fa fa-filter"></i>Kategori <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('beasiswa/form_kategori') ?>">Tambah Kategori</a></li>
							<li><a href="<?php echo base_url('beasiswa/tabel_kategori') ?>">Lihat Kategori</a></li>

						</ul>
					</li>
					<li><a><i class="fa fa-bullhorn"></i>Pengumuman <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<li><a href="<?php echo base_url('beasiswa/form_kategori') ?>">Upload Pengumuman</a></li>
							<li><a href="<?php echo base_url('beasiswa/tabel_kategori') ?>">Lihat Pengumuman</a></li>

						</ul>
					</li>
					<li><a href="<?php echo base_url('beasiswa/report') ?>"><i class="fa fa-bar-chart"></i> Laporan </a></li>

				</ul>
			</div>
		</div>


	</div>
</div>