<!-- Top Nav -->

<?php
defined('BASEPATH') or exit('No direct script access allowed');
$user = $this->db->get('tabel_user')->row_array();
?>
<div class="top_nav">
	<div class="nav_menu">
		<nav>
			<div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a></div>

			<ul class="nav navbar-nav navbar-right">
				<li>
					<a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<span class="mr-2 d-done d-lg-inline text-gray-600"><?php echo $user['nama']; ?></span>
						<img src="<?php echo base_url('assets/images/img.jpg') ?>" alt=""><span class="fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu pull-right">
						<li><a href="javascript:;"> Profil</a></li>
						<li><a href="<?php echo base_url('beasiswa/logout') ?>"><i class=" fa fa-sign-out pull-right"></i>Logout</a></li>
					</ul>
				</li>
			</ul>
			</li>
			</ul>
		</nav>
	</div>
</div>
<!-- /Top Nav -->