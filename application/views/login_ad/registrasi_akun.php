<!-- Simple login form -->
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="<?php echo base_url(); ?>" />
    <title>REGISTER SYSTEM | ADMIN</title>
    <link rel="icon" type="image/png" href="assets/images/ummy.ico">
    <!-- Global stylesheets -->
    <!-- <link href=" https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css"> -->
    <link href="assets/panel/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/panel/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="assets/panel/css/core.css" rel="stylesheet" type="text/css">
    <link href="assets/panel/css/components.css" rel="stylesheet" type="text/css">
    <link href="assets/panel/css/colors.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->
    <!-- Core JS files -->
    <script type="text/javascript" src="assets/panel/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/panel/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/panel/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/panel/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->
    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/panel/js/core/app.js"></script>
    <!-- /theme JS files -->
</head>

<body class="navbar-bottom login-container">
    <!-- Main navbar -->
    <div class="navbar navbar-inverse" style="background-color: #ba1919;">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo base_url('beasiswa/index') ?>"><b>PENDAFTARAN BEASISWA <label class="label label-success">Online</b></label> </a>
        </div>
    </div>
    <!-- /main navbar -->
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <!-- Main content -->
            <div class="content-wrapper">
                <form action="" method="post">
                    <div class="panel panel-body login-form">
                        <div class="text-center">
                            <img src="assets/images/logo_ummy.png" alt=" Logo" width="80">
                            <h5 class="content-group">REGISTER AKUN <small class="display-block"><b></b></small></h5>
                        </div>
                        <hr>
                        <?= $this->session->flashdata('pesan'); ?>
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">

                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="text" class="form-control" name="username" placeholder="Username">

                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left">
                            <input type="password" class="form-control" name="password" placeholder="Password">
                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>
                        <hr>
                        <div class="col-md-12">
                            <div class="form-group" style="text-align: center;">
                                <button type="submit" class="btn btn-danger "><a href="<?php echo base_url('beasiswa/login_ad') ?>" style="color: white;"><i class="glyphicon glyphicon-home "></i> <b>KEMBALI</b></a></button>
                                <button type="submit" class="btn btn-success "><a href="<?php echo base_url('beasiswa/registrasi_akun') ?>" style="color: white;"><i class=" glyphicon glyphicon-log-in"></i> <b>DAFTAR AKUN </b></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </div>
    <!-- /page container -->
    <!-- Footer -->
    <div class="navbar navbar-default navbar-fixed-bottom footer" style="background-color: #ba1919;">
        <div class="navbar-collapse collapse" id="footer">
            <div class="navbar-text" style="color: white;">
                <b>Copyright &copy; | </b>UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN - 2021
            </div>
        </div>
    </div>
    <!-- /footer -->

</html>
<!-- /simple login form -->