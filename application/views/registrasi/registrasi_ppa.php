<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="description" content="admin-themes-lab">
    <meta name="author" content="themes-lab">
    <base href="<?php echo base_url(); ?>" />
    <title>PENDAFTARAN BEASISWA PPA</title>
    <link rel="icon" href="assets/images/ummy.ico" type="image/x-icon" />
    <link href="assets/kitkat/assets/css/style.css" rel="stylesheet">
    <link href="assets/kitkat/assets/css/theme.css" rel="stylesheet">
    <link href="assets/kitkat/assets/css/ui.css" rel="stylesheet">
    <link href="assets/kitkat/assets/css/custom.css" rel="stylesheet">
    <link href="assets/kitkat/assets/plugins/font-awesome-animation/font-awesome-animation.min.css" rel="stylesheet">
    <link href="assets/kitkat/assets/input/css/fileinput.css" media="all" rel="stylesheet" type="text/css" />

    <!-- BEGIN PAGE STYLE -->
    <link href="assets/kitkat/assets/plugins/step-form-wizard/css/step-form-wizard.min.css" rel="stylesheet">
    <!-- END PAGE STYLE -->
    <script src="assets/kitkat/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<!-- LAYOUT: Apply "submenu-hover" class to body element to have sidebar submenu show on mouse hover -->
<!-- LAYOUT: Apply "sidebar-collapsed" class to body element to have collapsed sidebar -->
<!-- LAYOUT: Apply "sidebar-top" class to body element to have sidebar on top of the page -->
<!-- LAYOUT: Apply "sidebar-hover" class to body element to show sidebar only when your mouse is on left / right corner -->
<!-- LAYOUT: Apply "submenu-hover" class to body element to show sidebar submenu on mouse hover -->
<!-- LAYOUT: Apply "fixed-sidebar" class to body to have fixed sidebar -->
<!-- LAYOUT: Apply "fixed-topbar" class to body to have fixed topbar -->
<!-- LAYOUT: Apply "rtl" class to body to put the sidebar on the right side -->
<!-- LAYOUT: Apply "boxed" class to body to have your page with 1200px max width -->

<!-- THEME STYLE: Apply "theme-sdtl" for Sidebar Dark / Topbar Light -->
<!-- THEME STYLE: Apply  "theme sdtd" for Sidebar Dark / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltd" for Sidebar Light / Topbar Dark -->
<!-- THEME STYLE: Apply "theme sltl" for Sidebar Light / Topbar Light -->

<!-- THEME COLOR: Apply "color-default" for dark color: #2B2E33 -->
<!-- THEME COLOR: Apply "color-primary" for primary color: #319DB5 -->
<!-- THEME COLOR: Apply "color-red" for red color: #C9625F -->
<!-- THEME COLOR: Apply "color-green" for green color: #18A689 -->
<!-- THEME COLOR: Apply "color-orange" for orange color: #B66D39 -->
<!-- THEME COLOR: Apply "color-purple" for purple color: #6E62B5 -->
<!-- THEME COLOR: Apply "color-blue" for blue color: #4A89DC -->
<!-- BEGIN BODY -->



<body class="fixed-topbar sidebar-hover theme-sltl color-green">
    <section>
        <div class="main-content">
            <div class="topbar" style="background-color: #ba1919; color: #fff;">
                <div class="header-left">
                    <div class="col-sm-12">
                        <div style="margin-top:-8px;">
                            <h2>
                                <strong class="text-primary">
                                    <a href=""><span style="margin-left:40px;color:#fff;">PENDAFTARAN PPA</span></a>
                                </strong>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <!-- END TOPBAR -->
                <!-- BEGIN PAGE CONTENT -->
                <div class="page-content page-wizard">

                    <div class="header" style=" margin-top:-20px; text-align: center;">
                        <img src="assets/images/logo_ummy.png" style="margin-bottom:10px;" width="100"><br>
                        <h2 align="center">FORM PENDAFTARAN PPA <strong> <br>UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN</strong></h2>
                        <hr style="margin-top:20px;">

                    </div>
                    <fieldset>
                        <legend><b>Data Mahasiswa</b></legend>
                        <?php echo form_open_multipart('beasiswa/registrasi_ppa') ?>
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="panel">
                                    <div class="panel-heading" style="background:#ba1919; color: honeydew;">
                                        <h2 align="center" style="margin-top: 10px;">FORM PENGISIAN<br><b>PENDAFTARAN PPA</b> </h2>
                                        <!-- <hr> -->
                                    </div>
                                    <form class="daftar_ppa" method="post" action="<?= base_url('beasiswa/registrasi_ppa'); ?>">
                                        <div class=" panel-body">

                                            <div class="form-group" style="padding-bottom:30px;">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">N.P.M <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 prepend-icon" style="margin-top:1px;">
                                                    <input type="text" name="npm" id="npm" class="form-control" placeholder="Isikan NPM " required>
                                                    <i class="fa fa-users" style="margin-left:15px;"></i>
                                                    <div id="error-npm" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                    <div id="pesan_komentar">*Sesuai dengan NPM yang ditentukan jurusan</div>
                                                </div>
                                            </div>

                                            <div class="form-group" style="padding-bottom:30px;">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Nama Mahasiswa <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 prepend-icon">
                                                    <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" class="form-control" placeholder="Nama lengkap Mahasiswa" required>
                                                    <i class="fa fa-user" style="margin-left:15px;"></i>
                                                    <div id="error-nama_mahasiswa" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                    <div id="pesan_komentar">*Sesuai dengan akte kelahiran/ijazah</div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:-3px">Jenis Kelamin <span class="text-danger">*</span></label>
                                                <div class="col-sm-9">
                                                    <div class="radio" style="margin-top:3px;margin-left:-20px;">
                                                        <label>
                                                            <input type="radio" value="Laki-Laki" name="jenis_kelamin" id="jenis_kelamin"> <i class="fa fa-male"></i> &nbsp;Laki-laki
                                                        </label>
                                                    </div>
                                                    <div class="radio" style="margin-left:-20px;">
                                                        <label>
                                                            <input type="radio" value="Perempuan" name="jenis_kelamin" id="jenis_kelamin"> <i class=" fa fa-female"></i> &nbsp;Perempuan
                                                        </label>
                                                    </div>

                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Jurusan <span class="text-danger">*</span></label>
                                                <div class="col-sm-9" style="margin-top:3px;">
                                                    <select class="form-control" data-placeholder="Pilih Jurusan yang Diinginkan" name="prodi" id="prodi" required>
                                                        <option value="BAHASA INGGRIS">BAHASA INGGRIS</option>
                                                        <option value="BAHASA INDONESIA">BAHASA INDONESIA</option>
                                                        <option value="MATEMATIKA">MATEMATIKA</option>
                                                        <option value="BIOLOGI">BIOLOGI</option>
                                                        <option value="EKONOMI">EKONOMI</option>
                                                        <option value="AGRIBISNIS">AGRIBISNIS</option>
                                                        <option value="AGROTEKNOLOGI">AGROTEKNOLOGI</option>
                                                        <option value="PETERNAKAN">PETERNAKAN</option>
                                                        <option value="MANAJEMEN">MANAJEMEN</option>
                                                        <option value="MANAJEMEN INFORMATIKA">MANAJEMEN INFORMATIKA</option>
                                                        <option value="AKUNTANSI">AKUNTANSI</option>
                                                        <option value="HUKUM">HUKUM</option>
                                                    </select>
                                                    <div id="pesan_komentar">*Pilih jurusan yang diinginkan</div>
                                                    <div id="error-prodi" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Nilai IPK <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 " style="margin-top:3px;">
                                                    <input type="text" name="ipk" id="ipk" class="form-control" placeholder="Masukan Nilai IPK" required>
                                                    <div id="pesan_komentar">*Masukan Nilai sesuai dengan IPK masing-masing</div>
                                                    <div id="error-ipk" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Semester <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 " style="margin-top:3px;">
                                                    <input type="text" name="semester" id="semester" class="form-control" placeholder="Semester ke Berapa ?">
                                                    <div id="pesan_komentar">*Hanya untuk di atas Semester 2</div>
                                                    <div id="error-semester" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Tahun Akademik <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 " style="margin-top:3px;">
                                                    <input type="text" name="tahun_akademik" id="tahun_akademik" class="form-control" placeholder="Masukan Tahun Akademik ">

                                                    <div id="error-tahun_akademik" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" style="text-align:right; margin-top:6px">Surat Aktif Mahasiswa <span class="text-danger">*</span></label>
                                                <div class="col-sm-9 " style="margin-top:3px;">
                                                    <input type="file" name="surat_aktif_mhs" id="surat_aktif_mhs" class="form-control" required>
                                                    <div id="pesan_komentar">*Masukan file .jpg, ukuran max 300kb</div>
                                                    <div id="error-surat_aktif_mhs" style=" background:#FFBABA; color: #D8000C; width:auto; padding-left:10px; font-size: 10px;"></div>
                                                </div>
                                            </div>

                                        </div>
                                </div>
                                <div class="col-md-12">
                                    <div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group" style="text-align: right;">
                                            <button type="submit" id="simpan" class="btn btn-primary"><i class="fa fa-save"></i> <b>SIMPAN</b></a></button>
                                            <button type="submit" name="kembali" class="btn btn-danger"><a href="<?php echo base_url('beasiswa/index') ?>" style="color: white;"><i class="fa fa-home"></i> <b> KEMBALI</b></button>
                                        </div>
                                    </div>
                                    <?php echo form_close() ?>
                                </div>
                                </form>
                            </div>

                        </div>
                </div>
            </div>
            </form>
        </div>
        </div>

        <footer class="text-center">
            <div class="footer-below" style="background-color: #ba1919;">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12" style="text-align: center;">
                            PENDAFTARAN BEASISWA PPA &copy; UNIVERSITAS MAHAPUTRA MUHAMMAD YAMIN - <?php echo date('Y'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        </div>
        <!-- END PAGE CONTENT -->
        </div>
        <!-- END MAIN CONTENT -->
        <!-- BEGIN BUILDER -->
        <!-- END BUILDER -->
    </section>
    <!--sweetalert -->
    <a href=" #" class="scrollup"><i class="fa fa-angle-up"></i></a>

    <!-- jangan lupa menambahkan script js sweet alert di bawah ini  -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
    <script src="assets/kitkat/assets/plugins/jquery/jquery-1.11.1.min.js"></script>
    <script src="assets/kitkat/assets/plugins/jquery/jquery-migrate-1.2.1.min.js"></script>
    <script src="assets/kitkat/assets/plugins/jquery-ui/jquery-ui-1.11.2.min.js"></script>
    <script src="assets/kitkat/assets/plugins/gsap/main-gsap.min.js"></script>
    <script src="assets/kitkat/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/kitkat/assets/plugins/jquery-cookies/jquery.cookies.min.js"></script> <!-- Jquery Cookies, for theme -->
    <script src="assets/kitkat/assets/plugins/jquery-block-ui/jquery.blockUI.min.js"></script> <!-- simulate synchronous behavior when using AJAX -->
    <script src="assets/kitkat/assets/plugins/translate/jqueryTranslator.min.js"></script> <!-- Translate Plugin with JSON data -->
    <script src="assets/kitkat/assets/plugins/bootbox/bootbox.min.js"></script> <!-- Modal with Validation -->
    <script src="assets/kitkat/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script> <!-- Custom Scrollbar sidebar -->
    <script src="assets/kitkat/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script> <!-- Show Dropdown on Mouseover -->
    <script src="assets/kitkat/assets/plugins/charts-sparkline/sparkline.min.js"></script> <!-- Charts Sparkline -->
    <script src="assets/kitkat/assets/plugins/retina/retina.min.js"></script> <!-- Retina Display -->
    <script src="assets/kitkat/assets/plugins/select2/select2.min.js"></script> <!-- Select Inputs -->
    <script src="assets/kitkat/assets/plugins/icheck/icheck.min.js"></script> <!-- Checkbox & Radio Inputs -->
    <script src="assets/kitkat/assets/plugins/backstretch/backstretch.min.js"></script> <!-- Background Image -->
    <script src="assets/kitkat/assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> <!-- Animated Progress Bar -->
    <script src="assets/kitkat/assets/plugins/charts-chartjs/Chart.min.js"></script>
    <script src="assets/kitkat/assets/plugins/timepicker/jquery-ui-timepicker-addon.min.js"></script>
    <script src="assets/kitkat/assets/plugins/multidatepicker/multidatespicker.min.js"></script>
    <script src="assets/kitkat/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="assets/kitkat/assets/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js"></script>
    <!-- <script src="assets/kitkat/assets/js/builder.js"></script> Theme Builder -->
    <script src="assets/kitkat/assets/js/sidebar_hover.js"></script> <!-- Sidebar on Hover -->
    <script src="assets/kitkat/assets/js/application.js"></script> <!-- Main Application Script -->
    <script src="assets/kitkat/assets/js/plugins.js"></script> <!-- Main Plugin Initialization Script -->
    <script src="assets/kitkat/assets/js/widgets/notes.js"></script> <!-- Notes Widget -->
    <script src="assets/kitkat/assets/js/quickview.js"></script> <!-- Chat Script -->
    <script src="assets/kitkat/assets/js/pages/search.js"></script> <!-- Search Script -->
    <script src="assets/kitkat/js/cust.js"></script> <!-- Search Script -->
    <!-- BEGIN PAGE SCRIPTS -->
    <script src="assets/kitkat/assets/plugins/step-form-wizard/plugins/parsley/parsley.min.js"></script> <!-- OPTIONAL, IF YOU NEED VALIDATION -->
    <script src="assets/kitkat/assets/plugins/step-form-wizard/js/step-form-wizard.js"></script> <!-- Step Form Validation -->
    <script src="assets/kitkat/assets/js/pages/form_wizard.js"></script>
    <script src="assets/kitkat/assets/input/js/fileinput.js" type="text/javascript"></script>



    <script>
        $("#foto").fileinput({
            allowedFileExtensions: ['jpg'],
            showPreview: false,
            showUpload: false,
            browseClass: "btn btn-primary",
            elErrorContainer: "#errorBlock",
            maxFileSize: 2000,
            removeLabel: "Hapus",
            removeClass: "btn btn-danger",
            removeIcon: "<i class=\"glyphicon glyphicon-trash\"></i> "
        });
    </script>

    <script type="text/javascript">
        function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>
    <!-- END PAGE SCRIPTS -->
</body>

</html>