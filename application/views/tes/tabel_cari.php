<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<!-- Meta, title, CSS, favicons, etc. -->
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="<?php echo base_url('assets/images/ummy.ico') ?>" type="image/ico" />

	<title>Data Pendaftar Beasiswa PPA</title>

	<!-- Import CSS -->
	<link href="<?php echo base_url('vendors/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/nprogress/nprogress.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/iCheck/skins/flat/green.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/google-code-prettify/bin/prettify.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/select2/dist/css/select2.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/switchery/dist/switchery.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/starrr/dist/starrr.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/jqvmap/dist/jqvmap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') ?>" rel="stylesheet">


	<link href="<?php echo base_url('vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css') ?>" rel="stylesheet">

	<!-- PNotify -->
	<link href="<?php echo base_url('vendors/pnotify/dist/pnotify.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/pnotify/dist/pnotify.buttons.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/pnotify/dist/pnotify.nonblock.css') ?>" rel="stylesheet">


	<!-- FullCalendar -->
	<link href="<?php echo base_url('vendors/fullcalendar/dist/fullcalendar.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/fullcalendar/dist/fullcalendar.print.css') ?>" rel="stylesheet" media="print">


	<!-- Datatables -->
	<link href="<?php echo base_url('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') ?>" rel="stylesheet">
	<!-- Custom Theme Style -->
	<link href="<?php echo base_url('assets/css/custom.min.css') ?>" rel="stylesheet">
	<!-- /Import CSS -->

</head>

<body class="nav-md">
	<div class="container body" style="color: black;">
		<div class="main_container">
			<div class="col-md-3 left_col menu_fixed">
				<div class="left_col scroll-view" style="background: #ba1919;">
					<div class="navbar nav_title" style="border: 0; background:#ba1919;">
						<!-- logo info -->
						<div class="logo_pic">
							<a href=""><img src="<?php echo base_url('assets/images/logo_ummy.png') ?>" alt="..." class="img-circle logo_img"></a>
						</div>

					</div>
					<div class="profile">
						<a href="" class="site_title"><span style="font-size: 20px;"><?php echo 'Univesitas Mahaputra Muhammad Yamin' ?></span></a>
					</div>


					<div class="clearfix"></div>
					<!-- /menu profile quick info -->
					<br>
					<!-- Sidebar Menu -->
					<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
						<div class="menu_section">
							<h3></h3>
							<ul class="nav side-menu">

								<li><a href="<?php echo base_url('beasiswa/indexLog') ?>"><i class="fa fa-home"></i> Dashboard </a></li>
								<li><a href="<?php echo base_url('beasiswa/profil') ?>"><i class="fa fa-user"></i> Profil Admin </a></li>

								<li><a><i class="fa fa-database"></i> Data Mahasiswa <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo base_url('beasiswa/tabel_ppa') ?>">Beasiswa PPA</a></li>
										<li><a href="<?php echo base_url('beasiswa/tabel_bidikmisi') ?>">Beasiswa Bidik Misi</a></li>

									</ul>
								</li>
								<li><a><i class="fa fa-book"></i>Penerima Beasiswa <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo base_url('beasiswa/tabel_seleksi_bidikmisi') ?>">Seleksi Bidikmisi</a></li>
										<li><a href="<?php echo base_url('beasiswa/tabel_seleksi_ppa') ?>">Seleksi PPA</a></li>
									</ul>
								</li>
								<li><a><i class="fa fa-bullhorn"></i>Pengumuman <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo base_url('beasiswa/form_pengumuman') ?>">Upload Pengumuman</a></li>
										<li><a href="<?php echo base_url('beasiswa/tabel_pengumuman') ?>">Data Pengumuman</a></li>
									</ul>
								</li>
								<li><a><i class="fa fa-cog"></i>Dekan <span class="fa fa-chevron-down"></span></a>
									<ul class="nav child_menu">
										<li><a href="<?php echo base_url('beasiswa/form_dekan') ?>">Tambah Data Dekan</a></li>
										<li><a href="<?php echo base_url('beasiswa/tabel_dekan') ?>">Data Dekan</a></li>
									</ul>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>

			<?php
			defined('BASEPATH') or exit('No direct script access allowed');
			$user = $this->db->get('tabel_admin')->row_array();
			?>
			<div class="top_nav">
				<div class="nav_menu">
					<nav>
						<div class="nav toggle"><a id="menu_toggle"><i class="fa fa-bars"></i></a></div>

						<ul class="nav navbar-nav navbar-right">
							<li>
								<a class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
									<span class="mr-2 d-done d-lg-inline text-gray-600"><?php echo $user['nama']; ?></span>
									<img src="<?php echo base_url('assets/images/img.jpg') ?>" alt=""><span class="fa fa-angle-down"></span>
								</a>
								<ul class="dropdown-menu dropdown-usermenu pull-right">

									<li><a onclick="javascript: return confirm('Anda Yakin Ingin Keluar ?')" href="<?php echo base_url('beasiswa/logout') ?>"><i class=" fa fa-sign-out pull-right"></i>Logout</a></li>
								</ul>
							</li>
						</ul>
						</li>
						</ul>
					</nav>
				</div>
			</div>
			<!-- Page Content -->
			<div class="right_col" role="main">
				<div>
					<div class="page-title">
						<div class="title_left">

						</div>

						<div class="row">
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="x_panel">
									<div class="x_title">
										<h2>Lihat Data Pendaftar Beasiswa PPA</h2>
										<ul class="nav navbar-right panel_toolbox">
											<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
											</li>

											<li><a class="close-link"><i class="fa fa-close"></i></a>
											</li>
										</ul>
										<div class="clearfix">
										</div>
									</div>
									<div class="x_content">
										<?php if ($this->session->flashdata('ppa_added')) : ?>
											<button id="melinda" style="display: none;" class="btn btn-default source" onclick="new PNotify({
                                  title: 'Berhasil',
                                  text: '<?php echo $this->session->flashdata('ppa_added'); ?>',
                                  type: 'success',
                                  hide: false,
                                  styling: 'bootstrap3'
                              });">Success</button>
									</div>

								<?php endif; ?>
								<br>
								<form action="" method="get">
									<div class="input-group">

										<!-- menampilkan tahun menggunakan perulangan -->
										<?php
										$now = date('Y');
										echo "<select class='form-control' style='width:85px'  name='tahun_akademik' id='tahun_akademik'>";
										for ($a = 2012; $a <= $now; $a++) {
											echo "<option value='$a'>$a</option>";
										}
										echo "</select>";
										?>
										<button style="position: absolute;" class="btn btn-default" name="cari" value="cari" type="submit">Cari</button>
										<!-- menampilkan tahun menggunakan select biasa -->
										<!--select name="tahun_akademik" id="tahun_akademik">
				  <option value="2016">2016</option>
				  <option value="2017">2017</option>
				  <option value="2018">2018</option>
				  <option value="2019">2019</option>
				</select
	                <input type="text" class="form-control" name="tahun_akademik" placeholder="Masukan Kata Kunci..." -->

									</div>
								</form>

								<table id="datatable" class="table table-striped table-bordered" id="tabel_ppa">

									<a class="btn btn-danger" href="<?php echo base_url('laporan/filterlaporan_ppa') ?>"><i class="fa fa-print"></i>&nbsp;Print</a>
									<a class="btn btn-primary" href="<?php echo base_url('laporan/excel_ppa') ?>"><i class="fa fa-file"></i>&nbsp;Excel</a>
									<thead>
										<tr>
											<th>No</th>
											<th>Nama Mahasiswa</th>
											<th>NPM</th>
											<th>Jenis Kelamin</th>
											<th>Jurusan</th>
											<th>IPK</th>
											<th>Semester</th>
											<th>Tahun Akademik</th>
											<th style="text-align: center;"><i class="glyphicon glyphicon-cog  "></i></th>

										</tr>
									</thead>

									<?php $sn = 1 ?>
									<tbody>
										<?php foreach ($data as $row) { ?>
											<tr>
												<th scope="row"><?= $sn ?></th>
												<td><?= $row['nama_mahasiswa'] ?></td>
												<td><?= $row['npm'] ?></td>
												<td><?= $row['jenis_kelamin'] ?></td>
												<td><?= $row['prodi'] ?></td>
												<td><?= $row['ipk'] ?></td>
												<td><?= $row['semester'] ?></td>
												<td><?= $row['tahun_akademik'] ?></td>

												<td style=" text-align: center;">
													<?php echo anchor('beasiswa/edit_form_ppa/' . $row['id_ppa'], '<button class="btn btn-info btn-xs" type="button"><span class="fa fa-pencil"></span></button>'); ?>
													<i onclick="javascript: return confirm('Anda Yakin Ingin Menghapusnya ?')"> <?php echo anchor('beasiswa/remove_ppa/' . $row['id_ppa'], '<button  class="btn btn-danger btn-xs" type="button"><span class="fa fa-trash"></span></button>'); ?></i>
												</td>
											</tr>
											<?php $sn++; ?>
										<?php } ?>

									</tbody>

								</table>
								</div>
							</div>
						</div>

					</div>





				</div>
			</div>
		</div>
	</div>

	<!-- /Page Content -->
	<!-- Footer Content -->
	<footer>
		<div class="pull-right">
			<a href="#">Universitas Mahaputra Muhammad Yamin /</a>FEKOM UMMY | Copyright &copy;
		</div>
		<div class="clearfix"></div>
	</footer>
	<!-- /Footer Content -->
	</div>
	</div>

	<!-- Import Javascript -->
	<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jquery-ui/jquery-ui.js') ?>"></script>
	<script src="<?php echo base_url('vendors/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/fastclick/lib/fastclick.js') ?>"></script>
	<script src="<?php echo base_url('vendors/nprogress/nprogress.js') ?>"></script>

	<script src="<?php echo base_url('vendors/Chart.js/dist/Chart.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jquery-sparkline/dist/jquery.sparkline.min.js') ?>"></script>

	<script src="<?php echo base_url('vendors/morris.js/morris.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/raphael/raphael.min.js') ?>"></script>

	<script src="<?php echo base_url('vendors/gauge.js/dist/gauge.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/bootstrap-progressbar/bootstrap-progressbar.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/iCheck/icheck.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/skycons/skycons.js') ?>"></script>
	<!-- Flot -->
	<script src="<?php echo base_url('vendors/Flot/jquery.flot.js') ?>"></script>
	<script src="<?php echo base_url('vendors/Flot/jquery.flot.pie.js') ?>"></script>
	<script src="<?php echo base_url('vendors/Flot/jquery.flot.time.js') ?>"></script>
	<script src="<?php echo base_url('vendors/Flot/jquery.flot.stack.js') ?>"></script>
	<script src="<?php echo base_url('vendors/Flot/jquery.flot.resize.js') ?>"></script>
	<!-- Flot Plugins -->
	<script src="<?php echo base_url('vendors/flot.orderbars/js/jquery.flot.orderBars.js') ?>"></script>
	<script src="<?php echo base_url('vendors/flot-spline/js/jquery.flot.spline.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/flot.curvedlines/curvedLines.js') ?>"></script>
	<!-- JQVMap -->
	<script src="<?php echo base_url('vendors/jqvmap/dist/jquery.vmap.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jqvmap/dist/maps/jquery.vmap.world.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jqvmap/examples/js/jquery.vmap.sampledata.js') ?>"></script>

	<script src="<?php echo base_url('vendors/DateJS/build/date.js') ?>"></script>
	<!-- Bootstrap Date Range Picker -->
	<script src="<?php echo base_url('vendors/moment/min/moment.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
	<!-- bootstrap-datetimepicker -->
	<script src="<?php echo base_url('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') ?>"></script>



	<!-- FullCalendar -->
	<script src="<?php echo base_url('vendors/fullcalendar/dist/fullcalendar.min.js') ?>"></script>


	<!-- Ion.RangeSlider -->
	<script src="<?php echo base_url('vendors/ion.rangeSlider/js/ion.rangeSlider.min.js') ?>"></script>
	<!-- Bootstrap Colorpicker -->
	<script src="<?php echo base_url('vendors/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') ?>"></script>
	<!-- jquery.inputmask -->
	<script src="<?php echo base_url('vendors/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js') ?>"></script>
	<!-- jQuery Knob -->
	<script src="<?php echo base_url('vendors/jquery-knob/dist/jquery.knob.min.js') ?>"></script>


	<script src="<?php echo base_url('vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jquery.hotkeys/jquery.hotkeys.js') ?>"></script>
	<script src="<?php echo base_url('vendors/google-code-prettify/src/prettify.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jquery.tagsinput/src/jquery.tagsinput.js') ?>"></script>
	<script src="<?php echo base_url('vendors/switchery/dist/switchery.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/select2/dist/js/select2.full.min.js') ?>"></script>

	<!-- Validate -->
	<script src="<?php echo base_url('vendors/parsleyjs/dist/parsley.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/validator/validator.js') ?>"></script>

	<script src="<?php echo base_url('vendors/autosize/dist/autosize.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/starrr/dist/starrr.js') ?>"></script>
	<!-- Datatables -->
	<script src="<?php echo base_url('vendors/datatables.net/js/jquery.dataTables.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-buttons/js/buttons.flash.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-buttons/js/buttons.html5.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-buttons/js/buttons.print.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') ?>"></script>
	<script src="<?php echo base_url('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/jszip/dist/jszip.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/pdfmake/build/pdfmake.min.js') ?>"></script>
	<script src="<?php echo base_url('vendors/pdfmake/build/vfs_fonts.js') ?>"></script>
	<!-- Custom JS -->


	<!-- PNotify -->
	<script src="<?php echo base_url('vendors/pnotify/dist/pnotify.js') ?>"></script>
	<script src="<?php echo base_url('vendors/pnotify/dist/pnotify.buttons.js') ?>"></script>
	<script src="<?php echo base_url('vendors/pnotify/dist/pnotify.nonblock.js') ?>"></script>



	<script src="<?php echo base_url('assets/js/custom.min.js') ?>"></script>
	<!-- /Import Javascript -->


	<!-- Date Picker -->
	<script>
		$('#myDatepicker2').datetimepicker({
			format: 'DD-MM-YYYY',
			allowInputToggle: true
		});

		$('#myDatepicker3').datetimepicker({
			format: 'hh:mm A'
		});

		$('#myDatepicker4').datetimepicker({
			ignoreReadonly: true,
			allowInputToggle: true
		});

		$('#datetimepicker6').datetimepicker();

		$('#datetimepicker7').datetimepicker({
			useCurrent: false
		});

		$("#datetimepicker6").on("dp.change", function(e) {
			$('#datetimepicker7').data("DateTimePicker").minDate(e.date);
		});

		$("#datetimepicker7").on("dp.change", function(e) {
			$('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
		});
	</script>

	<!-- remove welcome notif pnotify and add notif -->
	<script>
		$(document).ready(function() {
			$('.ui-pnotify').remove();
			$('.source').trigger("click");

		});
	</script>

	<script>
		$(document).ready(function() {
			$("#tahun_akademik").change(function() {

			});
		});

		function tabel_ppa() {
			var tahun_akademik = $("#tahun_akademik").val();
			$.ajax({
				url: "<?= base_url('beasiswa/tabel_ppa') ?>",
				data: "tahun_akademik=" + tahun_akademik,
				success: function(data) {

				}

			});
		}
	</script>









</body>

</html>