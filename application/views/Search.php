<!DOCTYPE html>
<html>

<head>
	<title>Search Data</title>
</head>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

<body>

	<div class="container">
		<div class="row" style="margin-top: 50px">
			<div class="col-xs-4 col-xs-offset-4">
				<form action="<?= base_url('index.php/SearchController/index/') ?>" method="get">
					<div class="input-group">
						Tahun Akademik
						<!-- menampilkan tahun menggunakan perulangan -->
						<?php
						$now = date('Y');
						echo "<select name='tahun_akademik' id='tahun_akademik'>";
						for ($a = 2012; $a <= $now; $a++) {
							echo "<option value='$a'>$a</option>";
						}
						echo "</select>";
						?>

						<!-- menampilkan tahun menggunakan select biasa -->
						<!--select name="tahun_akademik" id="tahun_akademik">
				  <option value="2016">2016</option>
				  <option value="2017">2017</option>
				  <option value="2018">2018</option>
				  <option value="2019">2019</option>
				</select
	                <input type="text" class="form-control" name="tahun_akademik" placeholder="Masukan Kata Kunci..." -->
						<span class="input-group-btn">
							<button class="btn btn-default" type="submit">Cari</button>
						</span>
					</div>
				</form>
				<a class="btn btn-danger" href="<?= base_url('SearchModel/ambil_data') ?>"><i class="fa fa-print"></i>&nbsp;Print</a>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4 col-xs-offset-4 text-center">
				<h3>Data Barang</h3>
				<?php if (!empty($tahun_akademik)) { ?>
					<p style="color:orange"><b>Menampilkan data dengan kata kunci : "<?= $tahun_akademik; ?>"</b></p>
				<?php } ?>
				<table class="table">
					<thead>
						<tr>
							<th>Nama Mahasiswa</th>
							<th>NPM</th>
							<th>Jenis Kelamin</th>
							<th>Jurusan</th>
							<th>IPK</th>
							<th>Semester</th>
							<th>Tahun Akademik</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($data as $row) { ?>
							<tr>
								<td><?php echo $row['nama_mahasiswa'] ?></td>
								<td><?php echo $row['npm'] ?></td>
								<td><?php echo $row['jenis_kelamin'] ?></td>
								<td><?php echo $row['prodi'] ?></td>
								<td><?php echo $row['ipk'] ?></td>
								<td><?php echo $row['semester'] ?></td>
								<td><?php echo $row['tahun_akademik'] ?></td>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>

</html>