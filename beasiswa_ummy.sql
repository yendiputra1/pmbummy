-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Mar 2022 pada 12.01
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beasiswa_ummy`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_admin`
--

CREATE TABLE `tabel_admin` (
  `id_user` int(11) NOT NULL,
  `nrp` varchar(100) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `username` varchar(60) NOT NULL,
  `password` varchar(256) NOT NULL,
  `no_telp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_aktif` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tabel_admin`
--

INSERT INTO `tabel_admin` (`id_user`, `nrp`, `nama`, `username`, `password`, `no_telp`, `email`, `alamat`, `role_id`, `is_aktif`) VALUES
(14, '08627366', 'Muhammad Rafqi', 'Admin', '$2y$10$N/Bv9EI5jHd69ysmwtwKVO2hKSeCku3IgDDR5YZTp9U0FEUkf1URK', '08675434252', 'admirafqi34@gmail.com', 'Solok', 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_beasiswalainlain`
--

CREATE TABLE `tabel_beasiswalainlain` (
  `id_beasiswalain` int(4) NOT NULL,
  `nama_mahasiswa1` varchar(60) NOT NULL,
  `jenis_beasiswa` varchar(30) NOT NULL,
  `prodi1` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_beasiswalainlain`
--

INSERT INTO `tabel_beasiswalainlain` (`id_beasiswalain`, `nama_mahasiswa1`, `jenis_beasiswa`, `prodi1`) VALUES
(1, 'Nefara Wiranti', 'Beasiswa Lain-lain', 'Hukum');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_bidikmisi`
--

CREATE TABLE `tabel_bidikmisi` (
  `id_bidikmisi` int(10) NOT NULL,
  `no_pendaftaran` varchar(60) NOT NULL,
  `nama_lengkap` varchar(50) NOT NULL,
  `jk` varchar(30) NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL,
  `no_telp` varchar(60) NOT NULL,
  `alamat` text NOT NULL,
  `pekerjaan_ayah` varchar(50) NOT NULL,
  `pekerjaan_ibu` varchar(50) NOT NULL,
  `jml_tanggungan` int(12) NOT NULL,
  `penghasilan` int(60) NOT NULL,
  `asal_sekolah` varchar(50) NOT NULL,
  `tahun_tamat` varchar(50) NOT NULL,
  `rata2nilai` varchar(60) NOT NULL,
  `prestasi` varchar(60) NOT NULL,
  `prodi1` varchar(60) NOT NULL,
  `prodi2` varchar(60) NOT NULL,
  `thn_akademik` varchar(20) NOT NULL,
  `surat_keterangan_tdkmampu` varchar(256) NOT NULL,
  `foto_rumah` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_bidikmisi`
--

INSERT INTO `tabel_bidikmisi` (`id_bidikmisi`, `no_pendaftaran`, `nama_lengkap`, `jk`, `tanggal_lahir`, `no_telp`, `alamat`, `pekerjaan_ayah`, `pekerjaan_ibu`, `jml_tanggungan`, `penghasilan`, `asal_sekolah`, `tahun_tamat`, `rata2nilai`, `prestasi`, `prodi1`, `prodi2`, `thn_akademik`, `surat_keterangan_tdkmampu`, `foto_rumah`) VALUES
(6, '2021-25082021003', 'Vanitas Carte', 'Laki-Laki', '2000-04-04', '085269365841', 'Halaban', 'PNS', 'Ibu Rumah Tangga', 1, 1700000, 'SMA N 1 KOTA SOLOK', '2017', '70', '-', 'HUKUM', 'BIOLOGI', '2017', 'Rina_Agustin.jpg', 'Sunset_Ravens.jpg'),
(7, '2021-30082021004', 'Muhammad Rafqi', 'Laki-Laki', '1999-09-11', '082169826789', 'VI Suku', 'TNI', 'Ibu Rumah Tangga', 2, 3500000, 'SMA N 1 KOTA SOLOK', '2017', '81', '-', 'MANAJEMEN INFORMATIKA', 'EKONOMI', '2017', 'Rina_Agustin.jpg', 'Sunset_Ravens.jpg'),
(8, '2021-31082021005', 'Srima Yanti', 'Perempuan', '1998-03-03', '082545658955', 'Halaban', 'Petani', 'Petani', 1, 1450000, 'SMK N 1 KOTA SOLOK', '2017', '82.5', '-', 'MANAJEMEN', 'BIOLOGI', '2018', 'Rina_Agustin.jpg', 'Sunset_Ravens.jpg'),
(9, '2021-06092021006', 'Haruto Shiho', 'Laki-Laki', '2000-03-05', '082365987878', 'Simpang Rumbio', 'PNS', 'Ibu Rumah Tangga', 3, 3200000, 'SMA N 1 KOTA SOLOK', '2017', '87.3', '-', 'MANAJEMEN INFORMATIKA', 'MATEMATIKA', '2018', 'Rina_Agustin.jpg', 'Sunset_Ravens.jpg'),
(10, '2021-21092021007', 'Salsabila Ilna', 'Laki-Laki', '2000-05-04', '08562378675', 'VI Suku', 'TNI', 'Ibu Rumah Tangga', 4, 2700000, 'SMK N 1 KOTA SOLOK', '2018', '86.8', '-', 'MATEMATIKA', 'MANAJEMEN INFORMATIKA', '2020', 'Rina_Agustin.jpg', 'Sunset_Ravens.jpg'),
(12, '2021-03102021009', 'Muhammad Irsyad', 'Laki-Laki', '03-04-2000', '083456765567', 'VI Suku', 'POLRI', 'Ibu Rumah Tangga', 2, 4000000, 'SMA N 1 KOTA SOLOK', '2019', '83.6', '-', 'HUKUM', 'BIOLOGI', '2021', 'Biodata_Yanti.jpg', '20200815_110515.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_dekan`
--

CREATE TABLE `tabel_dekan` (
  `id_dekan` int(11) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `nama_dekan` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tabel_dekan`
--

INSERT INTO `tabel_dekan` (`id_dekan`, `nip`, `nama_dekan`) VALUES
(1, '8926762561', 'JUITA SUKRAINI, SE.MM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_pengumuman`
--

CREATE TABLE `tabel_pengumuman` (
  `id_pengumuman` int(11) NOT NULL,
  `name_file` varchar(255) NOT NULL,
  `waktu_upload` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tabel_pengumuman`
--

INSERT INTO `tabel_pengumuman` (`id_pengumuman`, `name_file`, `waktu_upload`) VALUES
(15, 'Hasil_seleksi_PPA.pdf', '2021-09-25 13:09:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_ppa`
--

CREATE TABLE `tabel_ppa` (
  `id_ppa` int(20) NOT NULL,
  `nama_mahasiswa` varchar(30) NOT NULL,
  `npm` varchar(30) NOT NULL,
  `prodi` varchar(30) NOT NULL,
  `ipk` float NOT NULL,
  `semester` varchar(10) NOT NULL,
  `tahun_akademik` varchar(20) NOT NULL,
  `jenis_kelamin` varchar(20) NOT NULL,
  `surat_aktif_mhs` varchar(265) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_ppa`
--

INSERT INTO `tabel_ppa` (`id_ppa`, `nama_mahasiswa`, `npm`, `prodi`, `ipk`, `semester`, `tahun_akademik`, `jenis_kelamin`, `surat_aktif_mhs`) VALUES
(5, 'Haviz Mahaputra', '201000457701002', 'HUKUM', 3.16, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(7, 'Salsa Ilmi', '201000457701003', 'MANAJEMEN INFORMATIKA', 3.44, '2', '2016', 'Perempuan', 'Enmado_House3.jpg'),
(8, 'Yudha Vansa', '201000457701019', 'MANAJEMEN INFORMATIKA', 3.12, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(9, 'Kota Hashima', '201000457701020', 'AKUNTANSI', 3, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(10, 'Ghina Andina', '201000457701029', 'AGROTEKNOLOGI', 3.6, '2', '2020', 'Laki-Laki', 'Enmado_House3.jpg'),
(11, 'Yuya Adami', '201000457701030', 'MANAJEMEN', 3, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(15, 'Harivan Sidiq', '201000457701023', 'AGRIBISNIS', 3.22, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(17, 'Hashifa Maryam', '201000457701032', 'AKUNTANSI', 3.55, '2', '2016', 'Perempuan', 'Enmado_House3.jpg'),
(19, 'Haruta Ryuuki', '201000457701066', 'MATEMATIKA', 3.3, '2', '2018', 'Laki-Laki', 'Enmado_House3.jpg'),
(29, 'Tenka Hiro', '201000457701080', 'MANAJEMEN INFORMATIKA', 3.71, '2', '2020', 'Laki-Laki', 'Enmado_House3.jpg'),
(30, 'Lili Sakura', '201000457701091', 'BIOLOGI', 3.65, '2', '2020', 'Laki-Laki', 'Enmado_House3.jpg'),
(31, 'Yumeno Hanzo', '201000457701092', 'PETERNAKAN', 3.23, '2', '2017', 'Laki-Laki', 'Enmado_House3.jpg'),
(32, 'Shina Mashiro', '201000457701081', 'MATEMATIKA', 3.18, '2', '2019', 'Laki-Laki', 'Enmado_House3.jpg'),
(38, 'Haru Wa Yuku', '201000457701101', 'BAHASA INDONESIA', 3.44, '2', '2016', 'Laki-Laki', 'Enmado_House3.jpg'),
(42, 'Hayame Ai', '201000457701001', 'HUKUM', 3.56, '2', '2020', 'Laki-Laki', 'Enmado_House3.jpg'),
(44, 'Mato Sakura', '201000457701008', 'BIOLOGI', 3.5, '2', '2016', 'Perempuan', 'Enmado_House3.jpg'),
(45, 'Alicia Heart', '201000457401076', 'BAHASA INGGRIS', 3.56, '4', '2020', 'Perempuan', 'Enmado_House3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(3, 'Admin2');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `tabel_admin`
--
ALTER TABLE `tabel_admin`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `tabel_beasiswalainlain`
--
ALTER TABLE `tabel_beasiswalainlain`
  ADD PRIMARY KEY (`id_beasiswalain`);

--
-- Indeks untuk tabel `tabel_bidikmisi`
--
ALTER TABLE `tabel_bidikmisi`
  ADD PRIMARY KEY (`id_bidikmisi`);

--
-- Indeks untuk tabel `tabel_dekan`
--
ALTER TABLE `tabel_dekan`
  ADD PRIMARY KEY (`id_dekan`);

--
-- Indeks untuk tabel `tabel_pengumuman`
--
ALTER TABLE `tabel_pengumuman`
  ADD PRIMARY KEY (`id_pengumuman`);

--
-- Indeks untuk tabel `tabel_ppa`
--
ALTER TABLE `tabel_ppa`
  ADD PRIMARY KEY (`id_ppa`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `tabel_admin`
--
ALTER TABLE `tabel_admin`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `tabel_beasiswalainlain`
--
ALTER TABLE `tabel_beasiswalainlain`
  MODIFY `id_beasiswalain` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tabel_bidikmisi`
--
ALTER TABLE `tabel_bidikmisi`
  MODIFY `id_bidikmisi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT untuk tabel `tabel_dekan`
--
ALTER TABLE `tabel_dekan`
  MODIFY `id_dekan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `tabel_pengumuman`
--
ALTER TABLE `tabel_pengumuman`
  MODIFY `id_pengumuman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `tabel_ppa`
--
ALTER TABLE `tabel_ppa`
  MODIFY `id_ppa` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
